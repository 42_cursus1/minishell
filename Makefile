# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: acloos <acloos@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/03/15 15:49:02 by acloos            #+#    #+#              #
#    Updated: 2023/11/21 11:54:27 by acloos           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #



# **************************************************************************** #
# ***************************** PROJECT'S NAME ******************************* #
# **************************************************************************** #

NAME					=	minishell
NAME_BONUS				=	minishell_bonus


# **************************************************************************** #
# *************************** FLAGS & COMMANDS ******************************* #
# **************************************************************************** #

CC						=	gcc
CFLAGS					=	-Wall -Wextra -Werror -g3 -MMD
RM						=	rm -rf


# **************************************************************************** #
# ********************************* SOURCES ********************************** #
# **************************************************************************** #

SRCSHELL				=	main env_utils splash misc_utils \
							minishell prompt signals signals_hd \
							ft_error ft_free_end ft_free_misc

SRCLEX					=	lexer lexing_quote_tokens lexing_tokens \
							lexing_word_tokens lexing_utils

SRCPARS					=	ast_nodes ast_nodes_cmd ast_utils stack \
							grammar_prod1 grammar_prod2 parser \
							expansion expansion_utils expansion_dol \
							expansion_quotes expansion_var_utils \
							heredoc_parse heredoc_write heredoc_file \

SRCEXEC					=	arguments exec exec_fork exec_access \
							exec_pipes_fds redir_utils redirs

SRCBULTIN				=	builtins_cd builtins_cd_utils builtins_echo\
							builtins_env builtins_exit builtins_pwd \
							builtins_export builtins_export_utils builtins_unset \
							check_builtins

SRC						=	$(addsuffix .c, $(addprefix srcs/lexing/, $(SRCLEX))) \
							$(addsuffix .c, $(addprefix srcs/parsing/, $(SRCPARS))) \
							$(addsuffix .c, $(addprefix srcs/executing/, $(SRCEXEC))) \
							$(addsuffix .c, $(addprefix srcs/builtins/, $(SRCBULTIN))) \
							$(addsuffix .c, $(addprefix srcs/shell/, $(SRCSHELL)))

SRCBONUS				=	$(addsuffix _bonus.c, $(addprefix srcs_bonus/lexing_bonus/, $(SRCLEX))) \
							$(addsuffix _bonus.c, $(addprefix srcs_bonus/parsing_bonus/, $(SRCPARS))) \
							$(addsuffix _bonus.c, $(addprefix srcs_bonus/executing_bonus/, $(SRCEXEC))) \
							$(addsuffix _bonus.c, $(addprefix srcs_bonus/builtins_bonus/, $(SRCBULTIN))) \
							$(addsuffix _bonus.c, $(addprefix srcs_bonus/shell_bonus/, $(SRCSHELL)))


# **************************************************************************** #
# ********************************** OBJECTS ********************************* #
# **************************************************************************** #

OBJ_DIR					=	obj/
OBJ						=	$(SRC:srcs/%.c=$(OBJ_DIR)%.o)

OBJ_BONUS_DIR			=	obj_bonus/
OBJ_BONUS				=	$(SRCBONUS:srcs_bonus/%.c=$(OBJ_BONUS_DIR)%.o)


# **************************************************************************** #
# ********************************* INCLUDES ********************************* #
# **************************************************************************** #

INCLS					=	-I ./incls/
INCLS_BONUS				=	-I ./incls_bonus/

LIBFT_PATH				=	./libft
LIBFT					=	$(LIBFT_PATH)/libft.a
LIBINCL					=	-L libft/ -lft -lreadline


# **************************************************************************** #
# ********************************** RULES *********************************** #
# **************************************************************************** #

$(OBJ_DIR)%.o:			srcs/%.c incls/minishell.h
						$(CC) $(CFLAGS) -I libft/sources -I srcs -c $< -o $@ $(INCLS)

$(OBJ_BONUS_DIR)%.o:	srcs_bonus/%.c incls_bonus/minishell_bonus.h
						$(CC) $(CFLAGS) -I libft/sources -I srcs_bonus -c $< -o $@ $(INCLS_BONUS) 

$(DEPS_DIR)%.d:			srcs/%.c incls/minishell.h
						$(CC) $(CFLAGS) -I libft/sources -I srcs -c $< -o $@

$(DEPS_DIR_BONUS)%.d:	srcs_bonus/%.c incls_bonus/minishell.h
						$(CC) $(CFLAGS) -I libft/sources -I srcs_bonus -c $< -o $@

all:					$(NAME) 
						@echo "\033[32m[Program is ready for use]\033[0m"

$(NAME):				$(LIBFT) $(OBJ_DIR) $(OBJ) $(DEPS_DIR)
						$(CC) $(CFLAGS) $(OBJ) -o $(NAME) $(LIBINCL) 
						@echo "\033[32m[minishell created]\033[0m"
						
$(NAME_BONUS):			$(LIBFT) $(OBJ_BONUS_DIR) $(OBJ_BONUS) $(DEPS_DIR_BONUS)
						$(CC) $(CFLAGS)  $(OBJ_BONUS) -o $(NAME_BONUS) $(LIBINCL)
						@echo "\033[32m[minishell_bonus created]\033[0m"

$(LIBFT):
						$(MAKE) -C $(LIBFT_PATH) all -s
						@echo "\033[32m[Libft created]\033[0m"

$(OBJ_DIR):
						mkdir -p $(OBJ_DIR)shell/
						mkdir -p $(OBJ_DIR)lexing/
						mkdir -p $(OBJ_DIR)parsing/
						mkdir -p $(OBJ_DIR)executing/
						mkdir -p $(OBJ_DIR)builtins/

$(OBJ_BONUS_DIR):
						mkdir -p $(OBJ_BONUS_DIR)shell_bonus/
						mkdir -p $(OBJ_BONUS_DIR)lexing_bonus/
						mkdir -p $(OBJ_BONUS_DIR)parsing_bonus/
						mkdir -p $(OBJ_BONUS_DIR)executing_bonus/
						mkdir -p $(OBJ_BONUS_DIR)builtins_bonus/

$(DEPS_DIR):			
						mkdir -p $(DEPS_DIR)

$(DEPS_DIR_BONUS):			
						mkdir -p $(DEPS_DIR_BONUS)

bonus:					$(NAME_BONUS)

clean:
						$(MAKE) -C $(LIBFT_PATH) clean
						$(RM) $(OBJ_DIR) $(OBJ_BONUS_DIR) $(DEPS_DIR)
						@echo "\033[33m[Cleaned up]\033[0m"

fclean:					clean
						$(MAKE) -C $(LIBFT_PATH) fclean
						$(RM) $(NAME) $(NAME_BONUS)
						@echo "\033[33m[Fully cleaned up]\033[0m"

re:						fclean
						make all
# pour compiler plus vite => make -j re

.PHONY:	all clean fclean re bonus libft
