/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_struct_bonus.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/12 15:47:21 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:32:19 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_STRUCT_BONUS_H
# define DATA_STRUCT_BONUS_H

# include <unistd.h>
# include "minishell_bonus.h"

/*
Below is the list of token types described in the BNF grammar
*/

typedef enum e_bnf
{
	E_WORD,
	E_PIPE,
	E_GREAT_OUTFILE,
	E_LESS_INFILE,
	E_DGREAT_OUTAPP,
	E_DLESS_HERE,
	E_REDIR_OP,
	E_AND_IF,
	E_OR_IF,
	E_EPSILON,
	E_NONTERMS,
	E_COMPLETE_CMD,
	E_LIST,
	E_PIPELINE,
	E_SIMPLE_CMD,
	E_CMD_PREFIX,
	E_CMD_WORD,
	E_CMD_SUFFIX,
	E_REDIR,
	E_FILENAME,
	E_BANG,
	E_TOKEN_END,
}	t_tkn_type;

typedef struct s_token
{
	t_tkn_type		tkn_type;
	char			*val;
	struct s_token	*nxt_tkn;
}	t_token;

typedef struct s_ast
{
	t_token			*tkn_node;
	t_tkn_type		tkn_type;
	struct s_ast	*parent;
	struct s_ast	*left;
	struct s_ast	*right;
}	t_ast;

typedef struct s_stack
{
	t_tkn_type		tkn_type;
	struct s_stack	*nxt_tkn;
}	t_stack;

typedef struct s_redir
{
	int				redir_type;
	int				redir_fd;
	char			*redir_path;
	struct s_redir	*next_redir;
}	t_redir;

typedef struct s_exec
{
	int		**fd_array;
	int		pipe_cnt;
	int		haz_redir;
	t_ast	*ast_pointer;
	pid_t	pid;
}	t_exec;

//t_token *parse_cursor :
//when parsing, this is the string cursor, not the TOS cursor

typedef struct s_data
{
	char			**env_baz;
	char			**env_cpy;
	char			*prompt;
	char			*cmd_line;
	char			*word;
	int				idx;
	int				lex_err;
	int				parsing_result;
	int				parse_err;
	int				exec_err;
	t_token			*tkn_start;
	t_token			*parse_cursor;
	t_ast			*root;
	t_ast			*current;
	t_stack			*parse_stack;
	t_exec			*exec;
}	t_data;

#endif
