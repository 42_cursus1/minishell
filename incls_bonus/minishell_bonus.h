/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_bonus.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:50:51 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 10:06:21 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_BONUS_H
# define MINISHELL_BONUS_H

# include "data_struct_bonus.h"
# include "ansi_colors_bonus.h"
# include "libft.h"
# include <errno.h>
# include <fcntl.h>
# include <readline/history.h>
# include <readline/readline.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/ioctl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <termios.h>
# include <unistd.h>

/******************************************************************************
*                                  # define                                   *
******************************************************************************/

# define PSONE " \033[1;3;41;30mMinis'hell\033[0m $ "

# define PSTWO " \033[1;30;103mmini-hd >\033[0m "

# define PSTHREE "#? "

# define PSFOUR "+ "

# define SYNTAX_ERR	"Back off NOW! Minishell shall accept no argument from you"

# define NO_TTY	"oopsie...not a tty. Minishell works only in interactive mode"

# define NO_MINISHELL	"How sad! Minishell could not be initialized"

/******************************************************************************
*                                   SHELL                                     *
******************************************************************************/

extern int	g_exit_status;

void		splash(void);
t_data		*hellish_startup(int argc, char **argv, char **env);
char		*get_prompt(void);
void		signal_setter(void);
void		signal_check(void);
void		signal_hd(int signal);
void		signal_check_hd(void);

/******************************************************************************
*                             GENERAL FUNCTIONS                               *
******************************************************************************/

//general
int			spacer(char c);
char		*env_value(t_data *data, char *str);
char		**env_getter(char **copy, char **env);
char		*ft_strjoin_bis(char *s1, char *s2);
int			ft_strlen_double(char **str);

//free
void		free_stuff(t_data *data);
void		free_stack(t_stack *stack);
void		free_char_array(char **str);
void		free_exec(t_exec *exec);
void		free_exit(t_data *data, char **cmd, int exit_status);
void		free_redir(t_redir *redir);
void		free_out(t_data *data);
void		free_ast(t_ast *node);
void		free_token_list(t_token *token);

//ft_error.c
void		parse_error(int *status);
void		lex_error(int *status);
void		exec_err(int *status);

//init_env
int			env_nll(t_data *data);
char		**env_copy(char **env);
char		*get_shlvl(char *var);

/******************************************************************************
*                                    LEXER                                    *
******************************************************************************/

int			lexer(t_data *data);

//	Creating the abstract syntax tree
t_ast		*ast_create_node(t_data *data, t_token *token, t_tkn_type tok_type);
void		ast_add_node(t_data *data, t_ast *new_node);
void		is_root_null(t_data *data, t_ast *new_node);
int			ast_redir_node(t_data *data, t_token *token);
int			ast_pipe_node(t_data *data, t_token *token);
int			ast_wordcmd_node(t_data *data, t_token *token);
int			ast_cmdsuf_node(t_data *data, t_token *token);

//	Tokenizer
void		add_token(t_data *data, t_tkn_type type, char *value);
int			is_operator(char c);
void		word_tknz(t_data *data);
int			quote_tknz(t_data *data);
void		redir_tknz(t_data *data);
void		pipe_tknz(t_data *data);
void		amper_tknz(t_data *data);

/******************************************************************************
*                                   PARSER                                    *
******************************************************************************/

int			parse(t_data *data);

// Heredoc
int			heredoc_parse(t_data *data, t_ast *node);
void		write_hd(t_data *data, char *line, int fd, char *delim);
char		*hd_xpand(t_data *data, char *line);
int			open_hd_file_new(char *filename);
char		*get_filename_new(t_data *data);

// Parsing stack
t_tkn_type	pop(t_stack **head);
t_stack		*push(t_tkn_type type, t_stack *head);
int			stack_thru(t_stack *stack);

// Grammar rules
int			cpl_cmd(t_token **cursor, t_data *data);
int			list_cmd(t_token **cursor, t_data *data);
int			pipeline(t_token **cursor, t_data *data);
int			smpl_cmd(t_token **cursor, t_data *data);
int			cmd_prefix(t_token **cursor, t_data *data);
int			cmd_word(t_token **cursor, t_data *data);
int			cmd_suffix(t_token **cursor, t_data *data);
int			redir(t_token **cursor, t_data *data);

// Expansion
int			wd_expansion(t_data *data, t_ast *node);
char		*join_strs(char *og_str, char *prefix, char *middle, char *suffix);
char		*dol_xpand(t_data *data, char *node_value, int dollar_idx, int len);
int			find_middle_len(char *node_value, int idx);
int			dollar_len(char *str, int len);
char		*expand_dollar_string(t_data *data, char *dol_str,
				int dol_idx, int len);
char		*rm_sg_quotes(char *node_value, int q_idx);
char		*rm_db_quotes(t_data *data, char *node_value, int q_idx);
int			only_quotes(t_ast *node);
int			tilde_xpand(t_data *data, t_ast *node);
int			check_varname(t_data *data, char *var_name);
int			chk_vchar(char vnc, int idx);

/******************************************************************************
*                                    EXEC                                     *
******************************************************************************/

int			exec(t_data *data);

// general functions
char		**get_av(t_ast *tree_exec);
int			add_var(t_data *data, char *var, int len);
int			get_forking(t_data *data);
int			fd_curfew(t_data *data, int idx, int old_i);

//builtins
int			sgl_builtins(t_data *data);
int			builtin_checker(t_data *data, char **cmd_full);
int			built_echo(char **cmd_full);
int			built_env(t_data *data, char **cmd_full);
int			built_pwd(t_data *data, char **cmd_full);
int			built_exit(t_data *data, char **cmd_full);
int			built_export(t_data *data, char **cmd_full);
int			built_unset(t_data *data, char **cmd_full);
int			built_cd(t_data *data, char **cmd_full);
char		*find_dots(t_data *data, char *curpath);
int			cd_error(t_data *data, char *curpath, char **cmd_full, int flag);
int			add_var(t_data *data, char *var, int len);
int			del_var(t_data *data, char *var, int len);

// pipes & redirections
int			fd_utils(t_data *data, int i, int old_i);
//int			handle_heredocs(t_ast *executor);
int			check_if_redir(t_data *data, t_ast *executor);
t_redir		*make_redir_list(t_data *data, t_redir *redir_lst,
				t_token *tkn_for_redir);
void		free_ll_close_fds(t_redir **redir_list);
char		**check_cmd_access(t_data *data, char **cmd);

#endif
