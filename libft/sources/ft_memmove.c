/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/10 09:35:56 by acloos            #+#    #+#             */
/*   Updated: 2023/05/10 16:52:50 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	const char	*castsrc;
	char		*castdest;
	size_t		i;

	i = 0;
	castsrc = src;
	castdest = dest;
	if (castdest < castsrc)
	{
		while (i < n)
		{
			castdest[i] = castsrc[i];
			i++;
		}
	}
	else
		while (n-- > 0)
			castdest[n] = castsrc[n];
	return (castdest);
}
