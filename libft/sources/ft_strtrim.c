/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/10 09:37:03 by acloos            #+#    #+#             */
/*   Updated: 2023/05/10 16:57:29 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	int		i;
	int		j;
	char	*neat;

	i = 0;
	j = ft_strlen(s1);
	if (!s1 || !set)
		return (NULL);
	if (*s1 && *set)
	{
		while (s1[i] && ft_strchr(set, s1[i]))
			i++;
		while (s1[j - 1] && ft_strrchr(set, s1[j - 1]) && j > i)
			j--;
	}
	neat = (char *)malloc(sizeof(char) * (j - i + 1));
	if (!neat)
		return (NULL);
	ft_strlcpy(neat, &s1[i], (j - i + 1));
	return (neat);
}
