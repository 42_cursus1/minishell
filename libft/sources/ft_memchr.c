/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/10 09:35:46 by acloos            #+#    #+#             */
/*   Updated: 2023/05/10 16:53:04 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char	*memarea;
	unsigned char		carac;

	memarea = (const unsigned char *) s;
	carac = (unsigned char)c;
	while (n > 0)
	{
		if (*memarea == carac)
			return ((void *)memarea);
		n--;
		memarea++;
	}
	return (NULL);
}
