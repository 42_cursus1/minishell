/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/10 09:35:59 by acloos            #+#    #+#             */
/*   Updated: 2023/05/10 16:52:43 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*memarea;

	memarea = (unsigned char *) s;
	while (n > 0)
	{
		*memarea = (unsigned char) c;
		memarea++;
		n--;
	}
	return (s);
}
