# Minishell


## Description
In this subject, we are tasked with created a basic shell. What this means is : 
	- coding in the shell a handful of builtin functions (cd , echo, env, exit, export, pwd, unset) - no distinction is made between builtins and special builtins
	- displaying a prompt, and having a functional history
	- being able to execute binaries that can be found within the $PATH
	- handle environment variables, as well as $?
	- handle single quotes : nothing should be interpreted within a set
	- handle double quotes : environment variables should be interpreted within a set of double quotes
	- the backslash is NOT a quoting character (it is considered a standard character)
	- the exit status is handled with one global variable
	- there is a minimal handling of signals (^C and ^\) as well as ^D
	- this shell handles pipes and redirections - no other operator is valid (|| and && can be tackled in the bonus part of the project, but were not done here)
		- therefore, this shell DOES NOT handle lists. Also : '#' and ';' are not special characters in this project
	- this only kind of expansion that must be handled in this project it the environment variables expansion. Any other kind (arithmetic...) is not part of the project
		- however, I could not determine for absolutely sure (say, 200% sure) whether the tilde is considered a variable holding the value of $HOME, or an alias for $HOME. So this shell expands the tilde
	- there is no functionalities for scripting (we do not handle any reserved word such as case/esac, for, while, etc) 	

Also, this is the very first group project for 42 students : we team up with one other student... and experiment all the good - and not so good sometimes - parts of working with someone else.


## Usage
If you want to try and use this basic shell, all you need to do is execute the Makefile. You can then use it juste like a shell - again, please keep in mind it has very minimal functionalities
If you want to check for memory leaks with valgrind, please note this project uses readline functions, known for leaking... you can use the readline.supp file as an option to valgrind, so that these leaks are filtered.

## Documentation
I will now take the time to organize the info gathered while working on this project, to provide as simple as possible explanation for the way this project was tackled and how we worked on it... stay tuned


