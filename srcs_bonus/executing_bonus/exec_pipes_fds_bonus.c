/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_pipes_fds_bonus.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/20 17:37:46 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 14:33:36 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/*
this closes fd's in the parent process
*/

int	fd_curfew(t_data *data, int idx, int old_i)
{
	int	ret;

	ret = 1;
	if (data->exec->pipe_cnt > 0)
	{
		if (idx == 0 && old_i == -1)
		{
			if (data->exec->fd_array[0][1])
				ret = close(data->exec->fd_array[0][1]);
		}
		else if (idx == data->exec->pipe_cnt)
		{
			if (data->exec->fd_array[old_i][0])
				ret = close(data->exec->fd_array[old_i][0]);
		}
		else
		{
			if (data->exec->fd_array[old_i][0])
				ret = close(data->exec->fd_array[old_i][0]);
			if (data->exec->fd_array[idx][1])
				ret = close(data->exec->fd_array[idx][1]);
		}
	}
	return (ret);
}

int	cmd_start(t_data *data, int i)
{
	int	ret;
	int	j;

	j = 0;
	ret = dup2(data->exec->fd_array[i][1], STDOUT_FILENO);
	ret = close(data->exec->fd_array[i][1]);
	while (j < data->exec->pipe_cnt)
	{
		ret = close(data->exec->fd_array[j][0]);
		if (j > 0)
			ret = close(data->exec->fd_array[j][1]);
		j++;
	}
	return (ret);
}

int	cmd_middle(t_data *data, int i, int old_i)
{
	int	ret;
	int	j;

	j = 1;
	if (data->exec->pipe_cnt > 1)
	{
		ret = dup2(data->exec->fd_array[old_i][0], STDIN_FILENO);
		ret = close(data->exec->fd_array[old_i][0]);
		ret = dup2(data->exec->fd_array[i][1], STDOUT_FILENO);
		ret = close (data->exec->fd_array[i][1]);
		while (j < data->exec->pipe_cnt)
		{
			if (i - 1 != j)
				ret = close(data->exec->fd_array[j][0]);
			if (i != j)
			{
				if (data->exec->fd_array[j][1])
					ret = close(data->exec->fd_array[j][1]);
			}
			j++;
		}
	}
	return (ret);
}

int	fd_utils(t_data *data, int i, int old_i)
{
	int	ret;

	ret = 1;
	if (data->exec->pipe_cnt > 0)
	{
		if (i == 0)
			ret = cmd_start(data, i);
		if (i == data->exec->pipe_cnt)
		{
			ret = dup2(data->exec->fd_array[old_i][0], STDIN_FILENO);
			ret = close(data->exec->fd_array[old_i][0]);
		}
		else if (i > 0)
			ret = cmd_middle(data, i, old_i);
	}
	if (ret < 0)
		perror("minishell : dup2/close issue ");
	return (ret);
}
