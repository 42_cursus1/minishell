/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 17:17:45 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:35:21 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/*
Regarding the following line :
else if (!isatty(STDIN_FILENO))
	-> it allows avoiding issues with /dev/random or /dev/urandom (thx Gael)
	-> can  also be written as : if (!isatty(0))
*/

int	heavenly_intervention(int argc)
{
	if (argc != 1)
	{
		write(2, BOLD B_D_RED, ft_strlen(BOLD B_D_RED));
		write(2, SYNTAX_ERR, ft_strlen(SYNTAX_ERR));
		write(2, RST, ft_strlen(RST));
		write(2, "\n", 1);
		return (-1);
	}
	else if (!isatty(STDIN_FILENO))
	{
		write(2, BOLD B_D_MAGENTA, ft_strlen(BOLD B_D_MAGENTA));
		write(2, NO_TTY, ft_strlen(NO_TTY));
		write(2, RST, ft_strlen(RST));
		write(2, "\n", 1);
		return (-1);
	}
	return (1);
}

static t_data	*init_data_env(t_data *data, char **env)
{
	g_exit_status = 0;
	data->env_baz = NULL;
	data->env_cpy = NULL;
	if (env[0] != NULL)
	{
		if (!data->env_baz)
			data->env_baz = env_copy(env);
		if (!data->env_cpy)
			data->env_cpy = env_copy(env);
	}
	else
		env_nll(data);
	data->lex_err = 2;
	data->parse_err = 2;
	data->exec_err = 2;
	data->idx = 0;
	data->parsing_result = 0;
	data->prompt = NULL;
	data->cmd_line = NULL;
	data->word = NULL;
	return (data);
}

t_data	*hellish_startup(int argc, char **argv, char **env)
{
	t_data	*data;

	(void)argv;
	if (argc != 1 || !isatty(STDIN_FILENO))
	{
		if (heavenly_intervention(argc) == -1)
			exit(EXIT_FAILURE);
	}
	data = (t_data *)malloc(sizeof(t_data) * 1);
	if (!data)
	{
		write(2, BOLD B_D_RED, ft_strlen(BOLD B_D_RED));
		write(2, NO_MINISHELL, ft_strlen(NO_MINISHELL));
		write(2, RST, ft_strlen(RST));
		write(2, "\n", 1);
		exit(EXIT_FAILURE);
	}
	data = init_data_env(data, env);
	return (data);
}
