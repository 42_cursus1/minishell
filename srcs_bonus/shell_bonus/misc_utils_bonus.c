/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misc_utils_bonus.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/26 19:26:18 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:35:24 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	spacer(char c)
{
	if (c == ' ' || c == '\n' || c == '\f' || c == '\t' || c == '\v'
		|| c == '\r')
		return (1);
	return (0);
}

char	*env_value(t_data *data, char *str)
{
	int		i;
	char	*value;
	char	*tmp;

	i = 0;
	if (!check_varname(data, str))
		return (NULL);
	while (data->env_cpy[i])
	{
		if (ft_strncmp(data->env_cpy[i], str, ft_strlen(str)) == 0
			&& data->env_cpy[i][ft_strlen(str)] == '=')
		{
			tmp = ft_substr(data->env_cpy[i], ft_strlen(str) + 1,
					ft_strlen(data->env_cpy[i]) - ft_strlen(str));
			value = ft_strdup(tmp);
			free(tmp);
			return (value);
		}
		i++;
	}
	return (NULL);
}

int	ft_strlen_double(char **str)
{
	int	x;

	x = 0;
	while (str[x] != NULL)
			x++;
	return (x);
}

char	*ft_strjoin_bis(char *s1, char *s2)
{
	char		*s;
	size_t		total;
	size_t		i;
	size_t		j;

	total = (ft_strlen(s1) + ft_strlen(s2));
	s = (char *)malloc(sizeof(char) * (total + 2));
	i = 0;
	if (s == NULL)
		return (NULL);
	while (s1 && s1[i])
	{	
		s[i] = s1[i];
		i++;
	}
	j = 0;
	while (s2[j])
		s[i++] = s2[j++];
	s[total] = '\n';
	s[total + 1] = '\0';
	free(s1);
	return (s);
}
