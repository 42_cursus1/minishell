/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 17:19:44 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:35:29 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

void	signal_ctlr_c(int signal)
{
	if (signal == SIGINT)
	{
		write(2, "\n", 1);
		rl_replace_line("", 0);
		rl_on_new_line();
		rl_redisplay();
		g_exit_status = 130;
	}
}

void	signal_ignore(int signal)
{
	if (signal == SIGINT)
	{
		write(2, "\n", 1);
		rl_replace_line("", 0);
		rl_on_new_line();
		g_exit_status = 130;
	}
	else if (signal == SIGQUIT)
	{
		write(2, "Quit (core dumped)\n", 19);
		rl_replace_line("", 0);
		rl_on_new_line();
		g_exit_status = 131;
	}
}

void	signal_check(void)
{
	signal(SIGINT, &signal_ignore);
	signal(SIGQUIT, &signal_ignore);
}

void	signal_setter(void)
{
	signal(SIGTSTP, SIG_IGN);
	signal(SIGINT, &signal_ctlr_c);
	signal(SIGQUIT, SIG_IGN);
}
