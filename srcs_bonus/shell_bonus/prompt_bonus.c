/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt_bonus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 17:23:44 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:35:27 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

char	*coloring_status(char *status, char *open, char *close)
{
	char	*opening;
	char	*mid_status;
	char	*color_status;

	if (g_exit_status == 0)
		opening = ft_strjoin(F_D_GREEN, open);
	else
		opening = ft_strjoin(F_D_YELLOW, open);
	mid_status = ft_strjoin(opening, status);
	free(status);
	free(opening);
	color_status = ft_strjoin(mid_status, close);
	free(mid_status);
	return (color_status);
}

char	*get_status(void)
{
	char	*get_status;
	char	*full_status;

	if (g_exit_status == 0)
		get_status = ft_strdup("0");
	else
		get_status = ft_itoa(g_exit_status);
	full_status = coloring_status(get_status, "[", "]");
	return (full_status);
}

char	*prompt_remain(char *prompt)
{
	char	*ascii_one;
	char	*ascii_two;

	ascii_one = ft_strjoin("\001", prompt);
	free(prompt);
	ascii_two = ft_strjoin(ascii_one, "\002");
	free(ascii_one);
	prompt = ft_strdup(ascii_two);
	free(ascii_two);
	return (prompt);
}

char	*get_prompt(void)
{
	char	*prompt;
	char	*status;

	status = get_status();
	prompt = ft_strjoin(status, PSONE);
	free(status);
	return (prompt);
}
