/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_utils_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/19 15:13:14 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 14:35:05 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/* 
bash gets its PATH value from /etc/login.defs
	in env -i bash, you can still execute binaries from their name
	if you echo $PATH, terminal will print a PATH

cat login.defs | grep ENV_SUPATH

 */

char	*env_trim(char *login_line)
{
	char	*i_path;
	char	*tmp_path;
	int		i;
	int		j;

	i = 0;
	j = 0;
	tmp_path = malloc(sizeof(char) * ft_strlen(login_line) + 1);
	while (login_line[i] != '=')
		i++;
	i++;
	while (login_line[i] != '\0')
	{
		tmp_path[j] = login_line[i];
		i++;
		j++;
	}
	tmp_path[j] = '\0';
	i_path = ft_strjoin("PATH=", tmp_path);
	free(tmp_path);
	free(login_line);
	return (i_path);
}

char	*get_init_path(void)
{
	char	*i_path;
	char	*login_line;
	int		fd;

	login_line = ft_strdup("");
	fd = open("/etc/login.defs", O_RDONLY);
	if (fd < 0)
	{
		perror("minishell : error opening : login.defs ");
		return (NULL);
	}
	while (1)
	{
		if (ft_strncmp(login_line, "ENV_SUPATH", 10) == 0)
			break ;
		free(login_line);
		login_line = get_next_line(fd);
	}
	close(fd);
	if (login_line == NULL)
		return (NULL);
	i_path = env_trim(login_line);
	return (i_path);
}

int	env_nll(t_data *data)
{
	char	*pwd_s;
	char	*save;
	char	*path_login;

	path_login = get_init_path();
	save = NULL;
	pwd_s = ft_strdup("PWD=");
	pwd_s = ft_strjoin(pwd_s, getcwd(NULL, 0));
	save = ft_strjoin_bis(save, pwd_s);
	save = ft_strjoin_bis(save, "SHLVL=1");
	save = ft_strjoin_bis(save, "OLDPWD=");
	save = ft_strjoin_bis(save, "HOME=");
	if (path_login != NULL)
		save = ft_strjoin_bis(save, path_login);
	data->env_baz = ft_split(save, '\n');
	data->env_cpy = ft_split(save, '\n');
	free(save);
	free(pwd_s);
	free(path_login);
	return (0);
}

char	*get_shlvl(char *var)
{
	int		x;
	int		start_var;
	char	*lvl;

	x = 0;
	lvl = ft_strdup(var);
	while (var[x] != '=')
		x++;
	x++;
	start_var = x;
	if (var[x] == 0)
		lvl[x] = 48;
	while (var[x + 1] != 0 && ft_isdigit(var[x]))
		x++;
	if (!ft_isdigit(var[x]))
	{
		lvl[start_var] = 48;
		lvl[start_var + 1] = '\0';
	}
	else
	{
		lvl[x] = var[x] + 1;
		lvl[x + 1] = '\0';
	}
	return (lvl);
}

char	**env_copy(char **env)
{
	char	**copy;
	int		i;

	i = 0;
	while (env[i] != NULL)
		i++;
	copy = (char **)malloc(sizeof(char *) * (i + 1));
	if (copy == NULL)
		return (NULL);
	i = 0;
	while (env[i] != NULL)
	{
		if (ft_strncmp(env[i], "SHLVL=", ft_strlen("SHLVL=")) == 0)
			copy[i] = get_shlvl(env[i]);
		else
			copy[i] = ft_strdup(env[i]);
		if (copy[i] == NULL)
		{
			free_char_array(copy);
			return (NULL);
		}
		i++;
	}
	copy[i] = NULL;
	return (copy);
}
