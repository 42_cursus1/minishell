/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_parse_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/15 14:19:55 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 10:25:30 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/*
	OG Heredoc should stop with either EOF, or a specified word ("delimiter")
	all quotes within heredoc must be closed
	in Bash --posix, you end a heredoc either with:
	- the delimiter
	- EOF, which is done by Ctrl-D, and bash will print a warning
	If you end with Ctrl-C, it will exit the heredoc, then print: ^C


	When parsing :
	store current word as DELIMITER
	got thru node until DELIMITER is found again, with readline()
	!! EOF is not acceptable in minishell
	if issue : return adequate error
	if not, store what was parsed for execution
	if all went well, return 1
	else, return -1
*/

int	hd_eof(t_data *data, char *tag, char *filename, int fd)
{
	write(2, "Mini-shell : warning : here-document delimited by ", 51);
	write(2, "end-of-file (wanted `", 22);
	ft_putstr_fd(tag, 2);
	write(2, "')\n", 4);
	free(filename);
	free_ast(data->root);
	close(fd);
	data->parse_err = -5;
	return (data->parse_err);
}

char	*hd_rm_quotes(char *delim)
{
	char	tag;
	char	*tmp;
	int		i;

	tmp = NULL;
	if (delim[0] != '\'' && delim[0] != '\"')
			tmp = ft_strdup(delim);
	else if (delim[0] == '\'' || delim[0] == '\"')
	{
		tag = delim[0];
		i = 0;
		while (delim[i + 1] != '\0')
			i++;
		if (delim[i] == tag)
			tmp = ft_substr(delim, 1, i - 1);
	}
	return (tmp);
}

int	hd_check(char *cur, char *delim)
{
	char	*unquoted_delim;

	unquoted_delim = hd_rm_quotes(delim);
	if (ft_strncmp(cur, unquoted_delim, (ft_strlen(unquoted_delim) + 1)) == 0)
	{
		free(unquoted_delim);
		free(cur);
		return (1);
	}
	else
	{
		free(unquoted_delim);
		return (0);
	}
}

int	hd_end(t_data *data, char *hd_line, char *f_name, int fd)
{
	if (hd_line == NULL)
		return (hd_eof(data, data->root->tkn_node->nxt_tkn->val, f_name, fd));
	if (g_exit_status == 130)
	{
		data->current = data->root;
		free(f_name);
		free(hd_line);
		free_ast(data->root);
		close (fd);
		data->parse_err = -5;
		return (-1);
	}
	return (1);
}

int	heredoc_parse(t_data *data, t_ast *node)
{
	char	*hd_line;
	char	*filename;
	int		fd;

	signal_check_hd();
	filename = get_filename_new(data);
	fd = open_hd_file_new(filename);
	if (fd == -1)
		return (-1);
	while (1)
	{
		hd_line = NULL;
		hd_line = readline(PSTWO);
		if (hd_end(data, hd_line, filename, fd) < 0)
			return (-1);
		if (hd_check(hd_line, node->tkn_node->nxt_tkn->val) == 1)
			break ;
		write_hd(data, hd_line, fd, node->tkn_node->nxt_tkn->val);
	}
	close(fd);
	free(node->tkn_node->nxt_tkn->val);
	node->tkn_node->nxt_tkn->val = ft_strdup(filename);
	free(filename);
	return (1);
}
