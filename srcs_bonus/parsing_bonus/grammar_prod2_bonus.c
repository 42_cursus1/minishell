/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grammar_prod2_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/24 15:12:09 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:34:46 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/*
Rule #08 : E_CMD_PREFIX		: E_REDIR E_CMD_PREFIX
Rule #09 : 					| E_EPSILON

Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error
*/

int	cmd_prefix(t_token **cursor, t_data *data)
{
	(void)cursor;
	(void)data;
	if (((*cursor)->tkn_type >= E_GREAT_OUTFILE
			&& (*cursor)->tkn_type <= E_REDIR_OP))
	{
		data->parse_stack = push(E_CMD_PREFIX, data->parse_stack);
		data->parse_stack = push(E_REDIR, data->parse_stack);
		ast_redir_node(data, (*cursor));
	}
	else if ((*cursor)->tkn_type == E_WORD)
		data->parse_stack = push(E_EPSILON, data->parse_stack);
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	return (1);
}

/*
rule #10
Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error
*/

int	cmd_word(t_token **cursor, t_data *data)
{
	if ((*cursor)->tkn_type == E_WORD)
	{
		data->parse_stack = push(E_WORD, data->parse_stack);
		ast_wordcmd_node(data, (*cursor));
	}
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	return (1);
}

/*
Rule #11 : E_CMD_SUFFIX		: E_REDIR E_CMD_SUFFIX
Rule #12 : 					| E_WORD E_CMD_SUFFIX
Rule #13 : 					| E_EPSILON

Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error
*/

int	cmd_suffix(t_token **cursor, t_data *data)
{
	if ((*cursor)->tkn_type >= E_GREAT_OUTFILE
		&& (*cursor)->tkn_type <= E_REDIR_OP)
	{
		data->parse_stack = push(E_CMD_SUFFIX, data->parse_stack);
		data->parse_stack = push(E_REDIR, data->parse_stack);
	}
	else if ((*cursor)->tkn_type == E_WORD)
	{
		data->parse_stack = push(E_CMD_SUFFIX, data->parse_stack);
		data->parse_stack = push(E_WORD, data->parse_stack);
		ast_cmdsuf_node(data, (*cursor));
	}
	else if ((*cursor)->tkn_type == E_PIPE
		|| (*cursor)->tkn_type == E_AND_IF
		|| (*cursor)->tkn_type == E_OR_IF
		|| (*cursor)->tkn_type == E_TOKEN_END)
		data->parse_stack = push(E_EPSILON, data->parse_stack);
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	return (1);
}

/*
Rule #14 : E_REDIR			: E_REDIR_OP
Rule #15 : E_REDIR_OP		: E_LESS_INFILE
Rule #16 : 					| E_GREAT_OUTFILE
Rule #17 : 					| E_DGREAT_OUTAPP
Rule #18 : 					| E_DLESS_HERE

Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error
*/

int	no_double_redir(t_token *tkn_for_redir)
{
	t_token	*tmp;

	tmp = tkn_for_redir;
	while (tmp->tkn_type != E_PIPE && tmp->tkn_type != E_TOKEN_END)
	{
		if (tmp->tkn_type >= E_GREAT_OUTFILE && tmp->tkn_type <= E_DLESS_HERE)
		{
			if (tmp->nxt_tkn->tkn_type >= E_GREAT_OUTFILE
				&& tmp->nxt_tkn->tkn_type <= E_DLESS_HERE)
				return (-1);
		}
		tmp = tmp->nxt_tkn;
	}
	return (1);
}

int	redir(t_token **cursor, t_data *data)
{
	if ((*cursor)->nxt_tkn->tkn_type == E_PIPE
		&& ((*cursor)->tkn_type >= E_LESS_INFILE
			&& (*cursor)->tkn_type < E_REDIR_OP))
		return (-2);
	if ((*cursor)->tkn_type == E_LESS_INFILE)
		data->parse_stack = push(E_LESS_INFILE, data->parse_stack);
	else if ((*cursor)->tkn_type == E_GREAT_OUTFILE)
		data->parse_stack = push(E_GREAT_OUTFILE, data->parse_stack);
	else if ((*cursor)->tkn_type == E_DGREAT_OUTAPP)
		data->parse_stack = push(E_DGREAT_OUTAPP, data->parse_stack);
	else if ((*cursor)->tkn_type == E_DLESS_HERE)
		data->parse_stack = push(E_DLESS_HERE, data->parse_stack);
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	if ((*cursor)->tkn_type >= E_GREAT_OUTFILE
		&& (*cursor)->tkn_type < E_REDIR_OP)
	{
		if (no_double_redir(data->tkn_start) == -1)
			return (data->parse_err = -6);
		if (ast_redir_node(data, (*cursor)) < 0)
			return (data->parse_err = -9);
	}
	return (1);
}

/*
Rule #01 : E_COMPLETE_CMD	: E_SIMPLE_CMD E_LIST
Rule #02 : E_LIST			: E_AND_IF E_PIPELINE
Rule #03 : 					| E_OR_IF E_PIPELINE
Rule #04 : 					| E_PIPELINE
Rule #05 : E_PIPELINE		: E_PIPE E_SIMPLE_CMD E_PIPELINE
Rule #06 : 					| E_EPSILON
Rule #07 : E_SIMPLE_CMD		: E_CMD_PREFIX E_CMD_WORD E_CMD_SUFFIX
Rule #08 : E_CMD_PREFIX		: E_REDIR E_CMD_PREFIX
Rule #09 : 					| E_EPSILON
Rule #10 : E_CMD_WORD		: E_WORD
Rule #11 : E_CMD_SUFFIX		: E_REDIR E_CMD_SUFFIX
Rule #12 : 					| E_WORD E_CMD_SUFFIX
Rule #13 : 					| E_EPSILON
Rule #14 : E_REDIR			: E_REDIR_OP
Rule #15 : E_REDIR_OP		: E_LESS_INFILE
Rule #16 : 					| E_GREAT_OUTFILE
Rule #17 : 					| E_DGREAT_OUTAPP
Rule #18 : 					| E_DLESS_HERE
*/
