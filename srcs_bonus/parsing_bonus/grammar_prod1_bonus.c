/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grammar_prod1_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/24 14:57:03 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:34:42 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/*
Rule #01 : E_COMPLETE_CMD	: E_SIMPLE_CMD E_LIST

Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error

*/

int	cpl_cmd(t_token **cursor, t_data *data)
{
	if ((*cursor)->tkn_type >= E_WORD && (*cursor)->tkn_type <= E_REDIR_OP)
	{
		data->parse_stack = push(E_LIST, data->parse_stack);
		data->parse_stack = push(E_SIMPLE_CMD, data->parse_stack);
	}
	else if ((*cursor)->tkn_type == E_AND_IF
		|| (*cursor)->tkn_type == E_OR_IF
		|| (*cursor)->tkn_type <= E_PIPE)
		data->parse_stack = push(E_LIST, data->parse_stack);
	else if ((*cursor)->tkn_type == E_TOKEN_END)
		return (1);
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	return (1);
}

/*
Rule #02 : E_LIST			: E_AND_IF E_PIPELINE	(bonus)
Rule #03 : 					| E_OR_IF E_PIPELINE	(bonus)
Rule #04 : 					| E_PIPELINE

Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error

*/

int	list_cmd(t_token **cursor, t_data *data)
{
	if ((*cursor)->tkn_type >= E_PIPE)
			data->parse_stack = push(E_PIPELINE, data->parse_stack);
	else if ((*cursor)->tkn_type == E_AND_IF)
	{
		data->parse_stack = push(E_PIPELINE, data->parse_stack);
		data->parse_stack = push(E_AND_IF, data->parse_stack);
	}
	else if ((*cursor)->tkn_type == E_OR_IF)
	{
		data->parse_stack = push(E_PIPELINE, data->parse_stack);
		data->parse_stack = push(E_OR_IF, data->parse_stack);
	}
	else if ((*cursor)->tkn_type == E_TOKEN_END)
		return (1);
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	return (1);
}

/*
Rule #05 : E_PIPELINE		: E_PIPE E_SIMPLE_CMD E_PIPELINE
Rule #06 : 					| E_EPSILON

Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error

*/

int	pipeline(t_token **cursor, t_data *data)
{
	if ((*cursor)->tkn_type >= E_WORD && (*cursor)->tkn_type <= E_REDIR_OP)
	{
		data->parse_stack = push(E_PIPELINE, data->parse_stack);
		data->parse_stack = push(E_SIMPLE_CMD, data->parse_stack);
		data->parse_stack = push(E_PIPE, data->parse_stack);
		ast_pipe_node(data, (*cursor));
	}
	else if ((*cursor)->tkn_type == E_TOKEN_END)
		data->parse_stack = push(E_EPSILON, data->parse_stack);
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	return (1);
}

/*
Rule #07 : E_SIMPLE_CMD		: E_CMD_PREFIX E_CMD_WORD E_CMD_SUFFIX

Returns:
	 1 if successful
	-1 in case of memory error
	-2 in case of suntax/parsing error

*/

int	smpl_cmd(t_token **cursor, t_data *data)
{
	if ((*cursor)->tkn_type == E_WORD || ((*cursor)->tkn_type
			>= E_GREAT_OUTFILE && (*cursor)->tkn_type <= E_REDIR_OP))
	{
		data->parse_stack = push(E_CMD_SUFFIX, data->parse_stack);
		data->parse_stack = push(E_CMD_WORD, data->parse_stack);
		data->parse_stack = push(E_CMD_PREFIX, data->parse_stack);
	}
	else
		return (-2);
	if (data->parse_stack == NULL)
		return (-1);
	return (1);
}
/*
Rule #01 : E_COMPLETE_CMD	: E_SIMPLE_CMD E_LIST
Rule #02 : E_LIST			: E_AND_IF E_PIPELINE
Rule #03 : 					| E_OR_IF E_PIPELINE
Rule #04 : 					| E_PIPELINE
Rule #05 : E_PIPELINE		: E_PIPE E_SIMPLE_CMD E_PIPELINE
Rule #06 : 					| E_EPSILON
Rule #07 : E_SIMPLE_CMD		: E_CMD_PREFIX E_CMD_WORD E_CMD_SUFFIX
Rule #08 : E_CMD_PREFIX		: E_REDIR E_CMD_PREFIX
Rule #09 : 					| E_EPSILON
Rule #10 : E_CMD_WORD		: E_WORD
Rule #11 : E_CMD_SUFFIX		: E_REDIR E_CMD_SUFFIX
Rule #12 : 					| E_WORD E_CMD_SUFFIX
Rule #13 : 					| E_EPSILON
Rule #14 : E_REDIR			: E_REDIR_OP
Rule #15 : E_REDIR_OP		: E_LESS_INFILE
Rule #16 : 					| E_GREAT_OUTFILE
Rule #17 : 					| E_DGREAT_OUTAPP
Rule #18 : 					| E_DLESS_HERE
*/
