/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/26 16:40:16 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:34:27 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/*
https://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html
Bash manual specifies that a variable is made of a name and a value
	name=value
but does not specify what makes a name valid or unvalid

In this minishell, variable names will conform to the POSIX.1-2008 standard
	This means that names :
		cannot begin with a digit
		can only contain uppercase letters, digits, and underscore
		therefore, cannot contain the '=' sign

POSIX.1-2008
Environment variable names used by the utilities in the Shell 
and Utilities volume of POSIX.1-2008 consist solely of 
	uppercase letters, 
	digits, 
	and the <underscore> ( '_' )
from the characters defined in Portable Character Set 
and do not begin with a digit. 
*/

/*

the '$' character can be followed by:
	- numeric character
	- a special parameter (@ * # ? - $ ! 0)
		we will use this in m-s :
			?
		we will NOT use these in m-s : 
			@ * # - ! 0 $
				(can't use $$ as m-s rules do not allow getpid() fct)
	- a valid 1st character of variable name
	- '('
	- '{'
if it's anything else, result is unspecified

	here, we will consider that '$' can only be followed by :
		- a valid 1st character of variable name
		- '?'
	Anything else will be considered an error
*/

char	*joiner(char *s1, char *s2)
{
	char	*joiner;

	if (!s1)
		return (s2);
	if (!s2)
		return (s1);
	joiner = ft_strjoin(s1, s2);
	free(s1);
	free(s2);
	return (joiner);
}

char	*join_strs(char *og_str, char *prefix, char *middle, char *suffix)
{
	char	*tmp;
	char	*final;

	free(og_str);
	tmp = joiner(prefix, middle);
	final = joiner(tmp, suffix);
	return (final);
}

int	find_middle_len(char *node_value, int idx)
{
	int		len;
	char	tag;

	len = idx + 1;
	if (node_value[idx] == '$')
	{
		if (node_value[idx + 1] == '?'
			|| ft_isdigit(node_value[idx + 1]))
			return (2);
		else
			len = dollar_len(node_value, len);
	}
	else if (node_value[idx] == '\''
		|| node_value[idx] == '\"')
	{
		tag = '\"';
		if (node_value[idx] == '\'')
			tag = '\'';
		while (node_value[len] && node_value[len] != tag)
			len++;
		len++;
	}
	len = len - idx;
	return (len);
}

int	expand_and_advance(t_data *data, t_ast *node, char *node_value, int i)
{
	char	*pfx;
	char	*mdl;
	char	*sfx;
	int		str_len;

	pfx = NULL;
	mdl = NULL;
	sfx = NULL;
	pfx = ft_substr(node_value, 0, i);
	str_len = find_middle_len(node_value, i);
	sfx = ft_substr(node_value, i + str_len,
			ft_strlen(node_value) - i - str_len);
	if (node_value[i] == '$')
		mdl = dol_xpand(data, node_value, i, str_len);
	else if (node_value[i] == '\'')
		mdl = rm_sg_quotes(node_value, i);
	else if (node_value[i] == '\"')
		mdl = rm_db_quotes(data, node_value, i);
	str_len = ft_strlen(pfx) + ft_strlen(mdl);
	node->tkn_node->val = join_strs(node_value, pfx, mdl, sfx);
	return (str_len);
}

int	wd_expansion(t_data *data, t_ast *node)
{
	int	i;

	i = 0;
	if (node->tkn_node->val == NULL)
	{
		data->parse_err = -2;
		return (data->parse_err);
	}
	if (node->tkn_node->val[0] == '~')
		i += tilde_xpand(data, node);
	while (node->tkn_node->val && node->tkn_node->val[i]
		&& data->parse_err >= 0)
	{
		if (node->tkn_node->val[i] == '$' || node->tkn_node->val[i] == '\''
			|| node->tkn_node->val[i] == '\"')
			i = expand_and_advance(data, node, node->tkn_node->val, i);
		else
			i++;
	}
	return (1);
}
