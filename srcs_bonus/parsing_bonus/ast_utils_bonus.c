/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast_utils_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/30 18:28:59 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:34:25 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/*
1/ initialize ast / create and initialize new node

2/ create nodes for : redirs / word(cmd) / word(suffix) / pipe / list(bonus)

3/ add nodes to ast

*/

void	is_root_null(t_data *data, t_ast *new_node)
{
	new_node->parent = NULL;
	data->root = new_node;
	data->current = new_node;
}

void	ast_add_node(t_data *data, t_ast *new_node)
{
	if (data->current->tkn_type == E_REDIR)
	{	
		new_node->left = data->current;
		new_node->left->parent = new_node;
		data->root = new_node;
		data->current = new_node;
	}
	else
	{
		new_node->right = data->current;
		new_node->right->parent = new_node;
		data->root = new_node;
		data->current = new_node;
	}
}

t_ast	*ast_create_node(t_data *data, t_token *token, t_tkn_type tok_type)
{
	t_ast	*new_ast_node;

	if (!token || tok_type < 0)
		return (NULL);
	new_ast_node = (t_ast *)malloc(sizeof(t_ast));
	if (!new_ast_node)
	{
		data->parse_err = -4;
		return (NULL);
	}
	new_ast_node->right = NULL;
	new_ast_node->tkn_node = token;
	new_ast_node->tkn_type = tok_type;
	if (tok_type == E_PIPE)
		new_ast_node->parent = NULL;
	if (tok_type != E_PIPE)
		new_ast_node->left = NULL;
	return (new_ast_node);
}
