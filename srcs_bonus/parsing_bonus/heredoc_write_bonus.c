/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_write_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/08 18:21:41 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 09:54:30 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	expanding_hd(t_data *data, char **line, int idx)
{
	int		len;
	char	*pfx;
	char	*mdl;
	char	*sfx;
	char	*hd_line;

	len = idx + 1;
	pfx = NULL;
	mdl = NULL;
	sfx = NULL;
	hd_line = *line;
	pfx = ft_substr(hd_line, 0, idx);
	if (hd_line[idx + 1] == '?'
		|| ft_isdigit(hd_line[idx + 1]))
			len = 2;
	else
		len = find_middle_len(hd_line, idx);
	mdl = dol_xpand(data, hd_line, idx, len);
	sfx = ft_substr(hd_line, idx + len, ft_strlen(hd_line) - idx - len);
	len = ft_strlen(pfx) + ft_strlen(mdl);
	hd_line = join_strs(hd_line, pfx, mdl, sfx);
	*line = hd_line;
	return (len);
}

char	*hd_xpand(t_data *data, char *line)
{
	int		i;

	i = 0;
	while (line[i])
	{
		if (line[i] == '$')
			i = expanding_hd(data, &line, i);
		i++;
	}
	return (line);
}

void	write_hd(t_data *data, char *line, int fd, char *delim)
{
	if (delim[0] != '\'' && delim[0] != '\"')
			line = hd_xpand(data, line);
	write(fd, line, ft_strlen(line));
	write(fd, "\n", 1);
	free(line);
}
