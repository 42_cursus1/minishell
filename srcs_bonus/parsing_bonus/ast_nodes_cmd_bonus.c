/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast_nodes_cmd_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 18:30:10 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:34:22 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

void	make_cmdsuf(t_data *data, t_ast *new_node)
{
	t_ast	*tmp;

	tmp = data->current->right;
	while (tmp->left)
		tmp = tmp->left;
	new_node->parent = tmp;
	tmp->left = new_node;
}

int	ast_cmdsuf_node(t_data *data, t_token *token)
{
	t_ast	*new_node;

	new_node = ast_create_node(data, token, E_CMD_SUFFIX);
	if (!new_node)
	{
		data->parse_err = -4;
		return (-1);
	}
	if (wd_expansion(data, new_node) < 0)
		return (-2);
	if (data->current->tkn_type == E_CMD_WORD && data->current->right == NULL)
	{
		new_node->parent = data->current;
		data->current->right = new_node;
	}
	else if (data->current->tkn_type == E_CMD_WORD
		&& data->current->right != NULL)
		make_cmdsuf(data, new_node);
	else
		return (-2);
	return (1);
}

int	ast_wordcmd_node(t_data *data, t_token *token)
{
	t_ast	*new_node;

	new_node = ast_create_node(data, token, E_CMD_WORD);
	if (!new_node)
	{
		data->parse_err = -4;
		return (-1);
	}
	if (wd_expansion(data, new_node) < 0)
		return (-2);
	if (data->root == NULL)
		is_root_null(data, new_node);
	else if (data->current->tkn_type == E_CMD_WORD && data->current->parent
		->tkn_type == E_PIPE && data->current->parent->right == NULL)
	{
		new_node->parent = data->current->parent;
		data->current->parent->right = new_node;
		data->current = new_node;
	}
	else if (data->current->tkn_type == E_REDIR
		|| data->current->tkn_type == E_CMD_WORD)
		ast_add_node(data, new_node);
	else
		return (-2);
	return (1);
}
