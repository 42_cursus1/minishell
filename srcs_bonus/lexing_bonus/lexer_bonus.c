/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 17:23:29 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:33:48 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

/* int	spacer(char c)
{
	if (c == ' ' || c == '\n' || c == '\f' || c == '\t' || c == '\v'
		|| c == '\r')
		return (1);
	return (0);
} */

void	ctrl_tknz(t_data *data)
{
	if (data->cmd_line[data->idx] == '|')
		pipe_tknz(data);
	else if (data->cmd_line[data->idx] == '&')
		amper_tknz(data);
	else if (data->cmd_line[data->idx] == '('
		|| data->cmd_line[data->idx] == ')'
		|| data->cmd_line[data->idx] == '!'
		|| data->cmd_line[data->idx] == ';'
		|| data->cmd_line[data->idx] == '`')
	{
		data->lex_err = -5;
		write(2, F_D_MAGENTA, ft_strlen(F_D_MAGENTA));
		write(2, "Minishell warning : operators ();! are not authorized", 53);
		write(2, RST, ft_strlen(RST));
		write(2, "\n", 1);
	}
}

void	make_tknz(t_data *data)
{
	if (is_operator(data->cmd_line[data->idx]) == 1 && data->lex_err >= 0)
		ctrl_tknz(data);
	else if (is_operator(data->cmd_line[data->idx]) == 2 && data->lex_err >= 0)
		redir_tknz(data);
	else if (data->cmd_line[data->idx] > 32
		&& data->cmd_line[data->idx] < 127 && data->lex_err >= 0)
		word_tknz(data);
}

int	lexer(t_data *data)
{
	data->lex_err = 1;
	if (data->cmd_line[0] == 0)
		return (2);
	while (data->idx < (int)ft_strlen(data->cmd_line) && data->lex_err >= 0)
	{
		while (spacer(data->cmd_line[data->idx]) && data->lex_err >= 0)
			data->idx++;
		if (data->cmd_line[data->idx] > 32
			&& data->cmd_line[data->idx] < 127 && data->lex_err >= 0)
			make_tknz(data);
		else
			break ;
	}
	free(data->cmd_line);
	if (data->lex_err < 0)
		return (data->lex_err);
	add_token(data, E_TOKEN_END, "token_end");
	return (data->lex_err);
}
