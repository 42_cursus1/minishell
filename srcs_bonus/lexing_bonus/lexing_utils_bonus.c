/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexing_utils_bonus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/24 14:27:20 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 14:33:56 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	wd_chk(char c)
{
	if (c == '\'' || c == '\"' || c == '|' || c == '<' || c == '>')
		return (0);
	return (1);
}

/*
Control operators :
	&
	&&
	|
	||
	(
	)
	;
	;;
	<new line>

Redirection operators :
	<
	>
	>|
	<<
	>>
	<&
	>&
	<<-
	<>

If character is #, this one and all following characters are considered comments,
	up until (but not including) the new line

For the purpose of minishell, we consider the following to be a control operator
	!
	` (backtick)

We handle '#' as a normal character
*/

int	is_operator(char c)
{
	char	*control_ope;
	char	*redirect_ope;

	control_ope = "&|();!`";
	redirect_ope = "<>";
	while (*control_ope)
	{
		if (*control_ope++ == c)
			return (1);
	}
	while (*redirect_ope)
	{
		if (*redirect_ope++ == c)
			return (2);
	}
	return (0);
}

void	tkn_add_back(t_data *data, t_token *new_node)
{
	t_token	*tmp;

	tmp = data->tkn_start;
	while (data->tkn_start && data->tkn_start->nxt_tkn)
		data->tkn_start = data->tkn_start->nxt_tkn;
	data->tkn_start->nxt_tkn = new_node;
	data->tkn_start = tmp;
}

void	add_token(t_data *data, t_tkn_type type, char *value)
{
	t_token	*new_node;

	new_node = (t_token *)malloc(sizeof(t_token));
	if (!new_node)
		data->lex_err = -1;
	new_node->nxt_tkn = NULL;
	if (type < E_WORD || type > E_TOKEN_END)
		data->lex_err = -2;
	new_node->tkn_type = type;
	if (!value)
		data->lex_err = -3;
	new_node->val = ft_strdup(value);
	if (!data->tkn_start)
		data->tkn_start = new_node;
	else
		tkn_add_back(data, new_node);
}
