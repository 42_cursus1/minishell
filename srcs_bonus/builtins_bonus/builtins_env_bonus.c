/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_env_bonus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/21 17:12:53 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 14:33:03 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	built_env(t_data *data, char **cmd_full)
{
	int		x;
	int		y;

	x = 0;
	if (cmd_full[1] != NULL)
	{
		free_char_array(cmd_full);
		write(2, "minishell : env : no options nor arguments allowed\n", 52);
		return (data->exec_err = -1);
	}
	while (data->env_cpy[x] != NULL)
	{
		y = 0;
		while (data->env_cpy[x][y] != '\0')
		{
			write(1, &(data->env_cpy)[x][y], 1);
			y++;
		}
		write(1, "\n", 1);
		x++;
	}
	free_char_array(cmd_full);
	return (0);
}
