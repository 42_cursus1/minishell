/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_pwd_bonus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/21 17:12:40 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 14:33:14 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	built_pwd(t_data *data, char **cmd_full)
{
	char	*buffer;

	buffer = getcwd(NULL, 0);
	if (buffer == NULL)
	{
		perror("minishell : pwd ");
		free_char_array(cmd_full);
		data->exec_err = -1;
		return (-2);
	}
	if (cmd_full[0])
	{
		write(1, buffer, strlen(buffer));
		write(1, "\n", 1);
		free(buffer);
	}
	free_char_array(cmd_full);
	return (0);
}
