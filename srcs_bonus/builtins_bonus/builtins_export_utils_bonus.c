/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_export_utils_bonus.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 15:17:13 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 14:33:11 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

char	**env_getter(char **copy, char **env)
{
	char	*mini_env;
	int		x;

	x = 0;
	mini_env = NULL;
	while (env[x] != NULL)
	{
		mini_env = ft_strjoin_bis(mini_env, env[x]);
		x++;
	}
	copy = ft_split(mini_env, '\n');
	free(mini_env);
	return (copy);
}
