/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_unset_bonus.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/21 17:13:27 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 14:33:17 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	check_var_unset(char *av)
{
	int	x;

	x = 0;
	while (av[x] != '\0')
	{
		if ((av[x] >= 'A' && av[x] <= 'Z') || av[x] == '_' || ft_isdigit(av[x]))
		{
			if (x == 0 && ft_isdigit(av[x]))
			{
				write(2, "minishell : unset : not a valid identifier\n", 44);
				return (-1);
			}
			x++;
		}
		else
		{
			write(2, "minishell : unset : not a valid identifier\n", 44);
			return (-1);
		}
	}
	return (x);
}

int	del_var(t_data *data, char *var, int len)
{
	char	*new_env;
	int		i;

	new_env = NULL;
	i = 0;
	while (data->env_cpy[i] != NULL)
	{
		if (ft_strncmp(data->env_cpy[i], var, len) == 0
			&& ft_strncmp(var, "SHLVL", ft_strlen("SHLVL")) == 0)
		{
			new_env = ft_strjoin_bis(new_env, "SHLVL=0");
			i++;
		}
		else if (ft_strncmp(data->env_cpy[i], var, len) != 0)
		{
			new_env = ft_strjoin_bis(new_env, data->env_cpy[i]);
			i++;
		}
		else if (ft_strncmp(data->env_cpy[i], var, len) == 0)
			i++;
	}
	free_char_array(data->env_cpy);
	data->env_cpy = ft_split(new_env, '\n');
	free(new_env);
	return (0);
}

int	unset_var(t_data *data, char **cmd_full)
{
	int	x;
	int	flag;
	int	error;

	x = 1;
	flag = 0;
	error = 0;
	while (cmd_full[x])
	{
		flag = check_var_unset(cmd_full[x]);
		if (flag == 0)
			write(2, "minishell : unset : not a valid identifier\n", 44);
		else if (flag > 0)
			del_var(data, cmd_full[x], flag);
		else if (flag == (-1))
			error = (-1);
		x++;
	}
	return (error);
}

int	built_unset(t_data *data, char **cmd_full)
{
	char	*lvl;

	lvl = NULL;
	if (cmd_full[1] == NULL)
	{
		free_char_array(cmd_full);
		return (0);
	}
	else if (unset_var(data, cmd_full) == -1)
	{
		free_char_array(cmd_full);
		return (data->exec_err = -1);
	}
	if (!data->env_cpy)
	{
		lvl = ft_strjoin_bis(lvl, "SHLVL=1");
		data->env_cpy = ft_split(lvl, '\n');
		free(lvl);
	}
	free_char_array(cmd_full);
	return (0);
}
