/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_echo_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/21 14:42:27 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 14:33:00 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	check_option(char **cmd, int i)
{
	int	j;

	j = 1;
	if (cmd[i][0] == '-' && cmd[i][1] == 'n')
	{
		while (cmd[i][j] == 'n')
			j++;
		if (cmd[i][j] == '\0')
			return (1);
	}
	return (0);
}

int	built_echo(char **cmd_full)
{
	int		x;
	int		n_flag;

	x = 1;
	if (cmd_full[x] == NULL)
	{
		free_char_array(cmd_full);
		write(1, "\n", 1);
		return (0);
	}
	n_flag = check_option(cmd_full, x);
	if (n_flag == 1)
		x++;
	while (cmd_full[x] != 0)
	{
		if (cmd_full[x][0] != '\0')
			write(1, cmd_full[x], ft_strlen(cmd_full[x]));
		x++;
		if (cmd_full[x] != NULL)
			write(1, " ", 1);
	}
	if (n_flag == 0)
		write(1, "\n", 1);
	free_char_array(cmd_full);
	return (0);
}
