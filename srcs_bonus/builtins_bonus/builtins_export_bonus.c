/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_export_bonus.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/21 17:13:18 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/21 14:57:10 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	check_var(char *av)
{
	int	x;

	x = 0;
	while (av[x] != '\0')
	{
		if ((av[x] >= 'A' && av[x] <= 'Z') || av[x] == '_' || ft_isdigit(av[x]))
		{
			if (x == 0 && ft_isdigit(av[x]))
			{
				write(2, "minishell : export : not a valid identifier\n", 45);
				return (-1);
			}
			x++;
		}
		else if (av[x] == '=' && x > 0)
			return (x);
		else
		{
			write(2, "minishell : export : not a valid identifier\n", 45);
			return (-1);
		}
	}
	return (0);
}

char	**sorting(char **copy, int len)
{
	char	*tmp;
	int		x;
	int		y;

	x = 0;
	while (x < len)
	{
		y = 0;
		while (y < len)
		{
			if (ft_strncmp(copy[x], copy[y], ft_strlen(copy[y])) <= 0)
			{
				tmp = copy[x];
				copy[x] = copy[y];
				copy[y] = tmp;
			}
			y++;
		}
		x++;
	}
	return (copy);
}

int	sort_env(char **copy, int len)
{
	int	x;

	x = 0;
	copy = sorting(copy, len);
	while (copy[x] != NULL)
	{
		write(1, "declare -x ", ft_strlen("declare -x "));
		write(1, copy[x], ft_strlen(copy[x]));
		write(1, "\n", 1);
		x++;
	}
	return (0);
}

int	export_var(t_data *data, char **cmd_full)
{
	int	x;
	int	flag;
	int	error;	

	x = 1;
	flag = 0;
	error = 0;
	while (cmd_full[x])
	{
		flag = check_var(cmd_full[x]);
		if (flag == 0)
			write(2, "minishell : export : not a valid identifier\n", 45);
		else if (flag > 0)
			add_var(data, cmd_full[x], flag);
		else if (flag == (-1))
			error = (-1);
		x++;
	}
	return (error);
}

int	built_export(t_data *data, char **cmd_full)
{
	char	**save_env;

	save_env = NULL;
	if (cmd_full[1] == NULL)
	{
		save_env = env_copy(data->env_cpy);
		free_char_array(cmd_full);
		sort_env(data->env_cpy, ft_strlen_double(data->env_cpy));
		free_char_array(data->env_cpy);
		data->env_cpy = env_copy(save_env);
		free_char_array(save_env);
		return (0);
	}
	else if (export_var(data, cmd_full) == -1)
	{
		free_char_array(cmd_full);
		return (data->exec_err = -1);
	}
	free_char_array(cmd_full);
	return (0);
}
