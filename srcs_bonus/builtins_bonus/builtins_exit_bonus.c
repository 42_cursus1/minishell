/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_exit_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/21 17:13:43 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/21 15:47:49 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell_bonus.h"

int	check_exit_arg(char *arg)
{
	int	x;

	x = 0;
	while (arg[x] != '\0')
	{
		if (ft_isdigit(arg[x]))
		{
			x++;
		}
		else
			return (-1);
	}
	x = ft_atoi(arg);
	if (x > 0 && x <= 255)
		return (g_exit_status = x);
	return (-1);
}

int	built_exit(t_data *data, char **cmd_full)
{
	if (cmd_full[1])
	{
		if (check_exit_arg(cmd_full[1]) == -1)
		{
			write(2, SLOW_BLINK F_D_YELLOW, ft_strlen(SLOW_BLINK F_D_YELLOW));
			write(2, "uh-uh you didn't say the magic word\n", 37);
			write(2, RST, ft_strlen(RST));
			g_exit_status = 2;
		}
	}
	if (data)
	{
		free_char_array(cmd_full);
		free_stuff(data);
		free_char_array(data->env_baz);
		free_char_array(data->env_cpy);
		free(data);
	}
	exit(g_exit_status);
}
