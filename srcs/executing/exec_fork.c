/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_fork.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 16:08:02 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/21 09:30:49 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	cmd_slash(t_data *data, char **cmd_full)
{
	char	*tmp;

	if ((cmd_full[0][0] == '.' && cmd_full[0][1] == '/')
		|| cmd_full[0][0] == '/')
	{
		if (access(cmd_full[0], X_OK) == 0)
		{
			execve(cmd_full[0], cmd_full, data->env_cpy);
			tmp = ft_strjoin("minishell: execve issue ", cmd_full[0]);
			perror(tmp);
			free(tmp);
			free_exit(data, cmd_full, g_exit_status = 2);
		}
		else
		{
			tmp = ft_strjoin("minishell: ", cmd_full[0]);
			perror(tmp);
			free(tmp);
			free_exit(data, cmd_full, g_exit_status = 126);
		}
	}
	return (0);
}

void	exec_child(t_data *data, t_ast *executor, int i, char **cmd)
{
	if (fd_utils(data, i, i - 1) < 0)
		free_exit(data, cmd, 1);
	if (check_if_redir(data, executor) == -1)
		free_exit(data, cmd, g_exit_status);
	cmd_slash(data, cmd);
	if (builtin_checker(data, cmd) != 2)
		free_exit(data, NULL, g_exit_status);
	cmd = check_cmd_access(data, cmd);
	if (cmd == NULL)
		free_exit(data, NULL, g_exit_status);
	execve(cmd[0], cmd, data->env_cpy);
	perror("minishell : execve issue ");
	free_exit(data, cmd, g_exit_status = 2);
}

int	exec_cmd(t_data *data, pid_t pid, t_ast *executor, int i)
{
	char	**cmd;

	cmd = NULL;
	if (pid == 0)
	{
		cmd = get_av(executor);
		exec_child(data, executor, i, cmd);
	}
	else
	{
		if (fd_curfew(data, i, i - 1) == -1)
		{
			perror("minishell : fd_curfew ");
			return (data->exec_err = -4);
		}
	}
	free(cmd);
	return (1);
}

int	fd_sag_wagon(int pid)
{
	int	status;

	status = 0;
	waitpid(pid, &status, 0);
	if (WIFEXITED(status))
		g_exit_status = WEXITSTATUS(status);
	while (wait(NULL) != -1)
	{
	}
	return (1);
}

int	get_forking(t_data *data)
{
	pid_t	pid;
	int		i;
	t_ast	*executor;

	i = 0;
	while (i <= data->exec->pipe_cnt)
	{
		if ((data->exec->ast_pointer->tkn_type == E_CMD_WORD))
			executor = data->exec->ast_pointer;
		else if (data->exec->ast_pointer->tkn_type == E_PIPE)
			executor = data->exec->ast_pointer->left;
		pid = fork();
		if (pid == -1)
		{
			perror("minishell : fork issue ");
			return (data->exec_err = -1);
		}
		if (exec_cmd(data, pid, executor, i) < 0)
			return (data->exec_err);
		if (data->exec->ast_pointer->tkn_type == E_PIPE)
			data->exec->ast_pointer = data->exec->ast_pointer->right;
		i++;
	}
	fd_sag_wagon(pid);
	return (data->exec_err);
}
