/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/24 15:36:07 by acloos            #+#    #+#             */
/*   Updated: 2023/11/15 15:35:40 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	got_redir(t_ast *tmp, int cnt)
{
	if (tmp->tkn_type == E_REDIR)
		cnt++;
	if (tmp->left)
		cnt = got_redir(tmp->left, cnt);
	if (tmp->right)
		cnt = got_redir(tmp->right, cnt);
	return (cnt);
}

int	pipe_count(t_ast *tmp)
{
	int		cnt;

	cnt = 0;
	if (tmp->tkn_type != E_PIPE)
		return (cnt);
	while (tmp->tkn_type == E_PIPE)
	{
		cnt++;
		tmp = tmp->right;
	}
	return (cnt);
}

int	**init_pipes(t_data *data, int pipe_cnt)
{
	int		**fd_array;
	int		i;

	i = 0;
	fd_array = (int **)malloc(sizeof(int *) * (pipe_cnt + 1));
	if (fd_array == NULL)
	{
		data->exec_err = -2;
		return (NULL);
	}
	while (i < pipe_cnt)
	{
		fd_array[i] = (int *)malloc(sizeof(int) * 2);
		if (fd_array[i] == NULL)
		{
			data->exec_err = -2;
			return (NULL);
		}
		pipe(fd_array[i]);
		i++;
	}
	fd_array[i] = NULL;
	return (fd_array);
}

t_exec	*init_exec(t_data *data)
{
	t_exec	*exec;
	t_ast	*tmp;

	data->exec_err = 1;
	exec = (t_exec *)malloc(sizeof(t_exec));
	if (!exec)
	{
		data->exec_err = -2;
		return (NULL);
	}
	tmp = data->root;
	exec->fd_array = NULL;
	exec->pipe_cnt = pipe_count(tmp);
	if (exec->pipe_cnt > 0)
		exec->fd_array = init_pipes(data, exec->pipe_cnt);
	exec->haz_redir = 0;
	exec->ast_pointer = data->root;
	exec->haz_redir = got_redir(tmp, exec->haz_redir);
	exec->ast_pointer = data->root;
	g_exit_status = 0;
	return (exec);
}

/*
Basically, only 2 cases for the exec function :
	- either we have a single command (E_CMD_WORD) that ALSO IS a builtin
	- or we have a pipe (E_PIPE or E_CMD_WORD)

so if we have E_CMD_WORD, we check if it's a builtin
	-> yes ? run it, and return exec_err (which was init'd to 1)
	-> no ? go to next step : get_forking
*/

int	exec(t_data *data)
{
	signal_check();
	if (data->root->tkn_type != E_CMD_WORD
		&& data->root->tkn_type != E_PIPE)
		return (data->exec_err = -5);
	data->exec = init_exec(data);
	if (data->exec == NULL || data->exec_err < 0)
	{
		g_exit_status = 10;
		if (data->exec_err == 1)
			data->exec_err = -5;
		data->exec->ast_pointer = data->root;
		return (data->exec_err);
	}
	if (data->exec->ast_pointer->tkn_type == E_CMD_WORD
		&& data->exec->haz_redir == 0)
	{
		if (sgl_builtins(data) < 0)
			return (data->exec_err);
	}
	data->exec->ast_pointer = data->root;
	data->exec_err = get_forking(data);
	data->exec->ast_pointer = data->root;
	return (data->exec_err);
}
