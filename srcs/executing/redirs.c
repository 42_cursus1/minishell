/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/18 18:48:10 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/16 10:19:20 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	fds_open_dup_close(t_redir *redir_lst, int std_fd, int flags)
{
	redir_lst->redir_fd = open(redir_lst->redir_path, flags, 0644);
	if (redir_lst->redir_fd == -1)
		return (-1);
	if (dup2(redir_lst->redir_fd, std_fd) == -1)
		return (-1);
	if (close(redir_lst->redir_fd) == -1)
		return (-1);
	return (1);
}

int	handle_fds(t_redir *redir_lst)
{
	int	fd_check;

	fd_check = 0;
	if (redir_lst->redir_type == E_LESS_INFILE
		|| redir_lst->redir_type == E_DLESS_HERE)
		fd_check = fds_open_dup_close(redir_lst, STDIN_FILENO, O_RDONLY);
	else if (redir_lst->redir_type == E_GREAT_OUTFILE)
		fd_check = fds_open_dup_close(redir_lst,
				STDOUT_FILENO, O_CREAT | O_WRONLY | O_TRUNC);
	else if (redir_lst->redir_type == E_DGREAT_OUTAPP)
		fd_check = fds_open_dup_close(redir_lst,
				STDOUT_FILENO, O_CREAT | O_WRONLY | O_APPEND);
	if (fd_check == -1)
	{
		write(2, redir_lst->redir_path, ft_strlen(redir_lst->redir_path));
		perror("minishell : dup2/close of redirection fds ");
		return (-1);
	}
	return (1);
}

int	execute_redirs(t_data *data, t_redir *redir_lst)
{
	while (redir_lst != NULL)
	{
		if (handle_fds(redir_lst) == -1)
		{
			data->exec_err = -1;
			return (-1);
		}
		redir_lst = redir_lst->next_redir;
	}
	free_ll_close_fds(&redir_lst);
	return (1);
}

int	check_if_redir(t_data *data, t_ast *executor)
{
	t_ast	*tmp_ast;
	t_redir	*redir_lst;
	int		executed;

	tmp_ast = executor;
	if (tmp_ast->left && tmp_ast->left->tkn_type == E_REDIR)
	{
		redir_lst = NULL;
		redir_lst = make_redir_list(data, redir_lst,
				tmp_ast->tkn_node->nxt_tkn);
		if (!redir_lst)
			return (-1);
		executed = execute_redirs(data, redir_lst);
		if (executed == -1)
		{
			g_exit_status = 0;
			free_ll_close_fds(&redir_lst);
			return (data->exec_err = -1);
		}
		if (executed == 2)
			return (g_exit_status = 0);
		free_ll_close_fds(&redir_lst);
	}
	return (1);
}
