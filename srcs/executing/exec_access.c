/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_access.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/08 16:22:04 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 09:31:39 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	**get_path(t_data *data)
{
	char	*path;
	char	**env_path;
	char	*pathname;

	pathname = ft_strdup("PATH");
	path = env_value(data, "PATH");
	env_path = ft_split(path, ':');
	free(path);
	free(pathname);
	return (env_path);
}

char	*do_access(char **path, char *cmd_zero)
{
	char	*tmp_path_i;
	char	*tmp_slash;
	char	*tmp_full;
	int		i;

	i = 0;
	while (cmd_zero[0] && path[i])
	{
		tmp_path_i = ft_strdup(path[i]);
		tmp_slash = ft_strjoin(tmp_path_i, "/");
		free(tmp_path_i);
		tmp_full = ft_strjoin(tmp_slash, cmd_zero);
		free(tmp_slash);
		if (access(tmp_full, X_OK) == 0)
		{
			cmd_zero = ft_strdup(tmp_full);
			free(tmp_full);
			return (cmd_zero);
		}
		free(tmp_full);
		i++;
	}
	return (NULL);
}

char	**check_cmd_access(t_data *data, char **cmd)
{
	char	**env_path;
	char	*acc;

	env_path = get_path(data);
	if (env_path == NULL)
		return (cmd);
	acc = do_access(env_path, cmd[0]);
	if (acc == NULL)
	{
		write(2, cmd[0], ft_strlen(cmd[0]));
		if (!(cmd[0][0]))
			write(2, "''", 2);
		write(2, " : command not found\n", 22);
		free_char_array(env_path);
		free_char_array(cmd);
		g_exit_status = 127;
		return (NULL);
	}
	free_char_array(env_path);
	cmd[0] = ft_strdup(acc);
	free(acc);
	return (cmd);
}
