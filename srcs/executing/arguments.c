/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arguments.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/20 14:53:43 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/20 08:09:18 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	arg_cnt_ast(t_ast *tree_cnt)
{
	int		i;
	t_token	*tmp;
	t_ast	*tmp_ast;

	i = 0;
	tmp_ast = tree_cnt;
	tmp = tmp_ast->tkn_node;
	if (!tmp_ast->right || tmp_ast->right->tkn_type != E_CMD_SUFFIX)
		return (0);
	while (tmp_ast->tkn_node->tkn_type == E_WORD)
	{
		i++;
		tmp_ast->tkn_node = tmp_ast->tkn_node->nxt_tkn;
	}
	tmp_ast->tkn_node = tmp;
	return (i);
}

char	**fill_cmd(t_token *tmp_tkn, char **cmd_full, int arg_cnt)
{
	int	i;

	i = 1;
	while (i < arg_cnt && tmp_tkn->tkn_type == E_WORD)
	{
		if (!tmp_tkn->val)
			tmp_tkn = tmp_tkn->nxt_tkn;
		cmd_full[i] = ft_strdup(tmp_tkn->val);
		tmp_tkn = tmp_tkn->nxt_tkn;
		i++;
	}
	cmd_full[i] = 0;
	return (cmd_full);
}

char	**get_av(t_ast *tree_exec)
{
	char	**cmd_full;
	int		arg_cnt;
	t_token	*tmp_tkn;

	arg_cnt = 0;
	if (!tree_exec)
		return (NULL);
	if (tree_exec->right && tree_exec->right->tkn_type == E_CMD_SUFFIX)
		arg_cnt = arg_cnt_ast(tree_exec);
	cmd_full = malloc(sizeof(char *) * (arg_cnt + 2));
	if (cmd_full == NULL)
		return (NULL);
	cmd_full[0] = ft_strdup(tree_exec->tkn_node->val);
	tmp_tkn = tree_exec->tkn_node->nxt_tkn;
	cmd_full = fill_cmd(tmp_tkn, cmd_full, arg_cnt);
	return (cmd_full);
}

char	*add_old_var(char *env_cpy, char *var, int len, int *db)
{
	char	*new_var;

	new_var = NULL;
	if (ft_strncmp(env_cpy, var, len) == 0)
	{
		if (ft_strncmp(env_cpy, var, len) == 0
			&& ft_strncmp(var, "SHLVL=", ft_strlen("SHLVL=")) == 0)
			new_var = get_shlvl(var);
		else
			new_var = ft_strdup(var);
		*db += 1;
		return (new_var);
	}
	return (NULL);
}

int	add_var(t_data *data, char *var, int len)
{
	char	*new_env;
	char	*new_var;
	int		i;
	int		db;

	i = 0;
	db = 0;
	new_env = NULL;
	while (data->env_cpy[i] != NULL)
	{
		new_var = add_old_var(data->env_cpy[i], var, len, &db);
		if (new_var != NULL)
			new_env = ft_strjoin_bis(new_env, new_var);
		else
			new_env = ft_strjoin_bis(new_env, data->env_cpy[i]);
		free(new_var);
		i++;
	}
	if (ft_strncmp(data->env_cpy[i - 1], var, len) != 0 && db == 0)
		new_env = ft_strjoin_bis(new_env, var);
	free_char_array(data->env_cpy);
	data->env_cpy = ft_split(new_env, '\n');
	free(new_env);
	return (0);
}
