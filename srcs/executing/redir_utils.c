/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/03 17:02:39 by acloos            #+#    #+#             */
/*   Updated: 2023/11/13 17:51:40 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	free_ll_close_fds(t_redir **redir_list)
{
	t_redir	*current;

	current = *redir_list;
	while (current != NULL)
	{
		if (current->redir_fd > 0)
			close(current->redir_fd);
		current = current->next_redir;
	}
	free_redir(*redir_list);
}

void	*init_error(t_data *data, t_redir *new_node, char *redir_value)
{
	data->exec_err = -1;
	write(2, "minishell : ", 13);
	write(2, redir_value, ft_strlen(redir_value));
	write(2, " is not a redirection token\n", 29);
	free(new_node);
	return (NULL);
}

t_redir	*init_redir_list(t_data *data, t_token *tkn_for_redir)
{
	t_redir	*new_node;

	new_node = (t_redir *)malloc(sizeof(t_redir));
	if (!new_node)
	{
		data->exec_err = -2;
		return (NULL);
	}
	new_node->next_redir = NULL;
	new_node->redir_type = tkn_for_redir->tkn_type;
	if (tkn_for_redir->tkn_type == E_WORD)
		new_node->redir_path = tkn_for_redir->val;
	else if (tkn_for_redir->tkn_type >= E_GREAT_OUTFILE
		&& tkn_for_redir->tkn_type <= E_DLESS_HERE)
		new_node->redir_path = tkn_for_redir->nxt_tkn->val;
	else if (tkn_for_redir->tkn_type == E_TOKEN_END)
	{
		new_node->redir_type = data->root->left->tkn_node->tkn_type;
		new_node->redir_path = data->root->left->tkn_node->nxt_tkn->val;
	}
	else
		return (init_error(data, new_node, tkn_for_redir->val));
	new_node->redir_fd = 0;
	return (new_node);
}

t_redir	*add_redir_node(t_data *data, t_token *tkn_for_redir)
{
	t_redir	*new_node;

	new_node = (t_redir *)malloc(sizeof(t_redir));
	if (!new_node)
	{
		data->exec_err = -2;
		return (NULL);
	}
	new_node->next_redir = NULL;
	new_node->redir_fd = 0;
	if (tkn_for_redir->tkn_type >= E_GREAT_OUTFILE
		&& tkn_for_redir->tkn_type <= E_DLESS_HERE)
	{
		new_node->redir_type = tkn_for_redir->tkn_type;
		new_node->redir_path = tkn_for_redir->nxt_tkn->val;
	}
	else
	{
		data->exec_err = -5;
		return (NULL);
	}
	return (new_node);
}

t_redir	*make_redir_list(t_data *data, t_redir *redir_lst, t_token *redir_tkn)
{
	t_redir	*current;

	current = redir_lst;
	current = init_redir_list(data, redir_tkn);
	if (!current)
		return (NULL);
	redir_lst = current;
	if (redir_tkn->tkn_type < E_TOKEN_END)
		redir_tkn = redir_tkn->nxt_tkn;
	while (redir_tkn->tkn_type != E_PIPE
		&& redir_tkn->tkn_type != E_TOKEN_END)
	{
		if (redir_tkn->tkn_type != E_WORD)
		{
			current->next_redir = add_redir_node(data, redir_tkn);
			if (!current->next_redir)
			{
				free_ll_close_fds(&redir_lst);
				return (NULL);
			}
		current = current->next_redir;
		}
		redir_tkn = redir_tkn->nxt_tkn;
	}
	return (redir_lst);
}
