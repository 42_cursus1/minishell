/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/07 16:09:06 by acloos            #+#    #+#             */
/*   Updated: 2023/11/14 10:46:49 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*get_filename_new(t_data *data)
{
	char		*index;
	char		*filename;
	char		*home;
	char		*tmp;
	static int	idx = 0;

	index = ft_itoa(idx);
	home = env_value(data, "HOME");
	tmp = ft_strjoin(home, "/.tmp_hd/hd_");
	free(home);
	filename = ft_strjoin(tmp, index);
	free(index);
	free(tmp);
	if (!filename)
		return (NULL);
	idx++;
	return (filename);
}

int	open_hd_file_new(char *filename)
{
	int	fd;

	if (!filename)
		return (-1);
	fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (fd < 0)
	{
		perror("minishell : could not open file for heredoc ");
		free(filename);
		return (-1);
	}
	if (access(filename, F_OK) != 0)
	{
		perror("minishell : could not access file for heredoc ");
		free(filename);
		return (-1);
	}
	return (fd);
}
