/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion_dol.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/09 14:39:37 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 13:51:39 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*replace_with_value(t_data *data, char *var_name)
{
	char	*real_var;
	char	*var_value;

	real_var = NULL;
	var_value = env_value(data, var_name);
	real_var = ft_strdup(var_value);
	free(var_name);
	free(var_value);
	return (real_var);
}

int	can_follow_varname(char c)
{
	char	*follow_var;
	int		i;

	i = 0;
	follow_var = "/\'\"$~@#%%^*-+=[]{}?:,./? ";
	while (follow_var[i])
	{
		if (c == follow_var[i])
			return (1);
		i++;
	}
	return (0);
}

char	*expand_dollar_string(t_data *data, char *dol_str, int dol_idx, int len)
{
	char	*middle;
	char	*var_name;

	middle = NULL;
	if (dol_str[dol_idx + 1] == '?')
	{
		middle = ft_itoa(g_exit_status);
	}
	else if (ft_isdigit(dol_str[dol_idx + 1]))
		return (NULL);
	else if (dol_str[dol_idx + 1] && chk_vchar(dol_str[dol_idx + 1], 0) == 1)
	{
		var_name = ft_substr(dol_str, dol_idx + 1, len - 1);
		middle = replace_with_value(data, var_name);
		if (middle == NULL)
			return (NULL);
	}
	else if (can_follow_varname(dol_str[dol_idx + 1])
		|| dol_str[dol_idx + 1] == 0)
	{
		middle = ft_strdup("$");
	}
	return (middle);
}

char	*dol_xpand(t_data *data, char *node_value, int dollar_idx, int len)
{
	char	*middle;

	middle = expand_dollar_string(data, node_value, dollar_idx, len);
	return (middle);
}

int	dollar_len(char *str, int len)
{
	while (str[len])
	{
		if (can_follow_varname(str[len]))
			break ;
		len++;
	}
	return (len);
}
