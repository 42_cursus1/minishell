/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/02 17:39:50 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 12:54:25 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	check_varname(t_data *data, char *var_name)
{
	int	i;

	i = 1;
	(void)data;
	if (var_name[0] >= '0' && var_name[0] <= '9')
		return (0);
	while (var_name[i])
	{
		if (var_name[i] < 'A' || var_name[i] > 'Z')
		{
			if (var_name[i] < '0' || var_name[i] > '9')
			{
				if (var_name[i] != '_' && var_name[i] != '$'
					&& var_name[i + 1] != ' ')
				{
					return (0);
				}
			}
		}
		i++;
	}
	return (1);
}

int	chk_vchar(char vnc, int idx)
{
	if (idx == 0 && (vnc >= '0' && vnc <= '9'))
		return (0);
	if (vnc < 'A' || vnc > 'Z')
	{
		if (vnc < '0' || vnc > '9')
		{
			if (vnc != '_')
				return (-1);
		}
	}
	return (1);
}
