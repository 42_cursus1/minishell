/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 14:49:07 by acloos            #+#    #+#             */
/*   Updated: 2023/11/10 13:12:51 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_tkn_type	pop(t_stack **head)
{
	t_tkn_type		result;
	t_stack			*tmp;

	if ((*head) == NULL)
		return (E_TOKEN_END);
	tmp = (*head)->nxt_tkn;
	result = (*head)->tkn_type;
	free((*head));
	*head = tmp;
	return (result);
}

t_stack	*push(t_tkn_type type, t_stack *head)
{
	t_stack	*new_node;

	new_node = (t_stack *)malloc(sizeof(t_stack));
	if (!new_node)
		return (NULL);
	new_node->tkn_type = type;
	if (head)
		new_node->nxt_tkn = head;
	else
		new_node->nxt_tkn = NULL;
	return (new_node);
}

int	stack_thru(t_stack *stack)
{
	int	i;

	i = 0;
	while (stack)
	{
		i++;
		stack = stack->nxt_tkn;
	}
	return (i);
}
