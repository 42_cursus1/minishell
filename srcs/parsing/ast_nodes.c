/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast_nodes.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/05 13:53:31 by acloos            #+#    #+#             */
/*   Updated: 2023/11/14 12:17:34 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	make_pipe_node(t_data *data, t_ast *new_node)
{
	data->current->parent->right = new_node;
	new_node->parent = data->current->parent;
	new_node->left = data->current;
	new_node->left->parent = new_node;
}

int	ast_pipe_node(t_data *data, t_token *token)
{
	t_ast	*new_node;

	new_node = ast_create_node(data, token, E_PIPE);
	if (!new_node)
	{
		data->parse_err = -4;
		return (-1);
	}
	if (data->current->tkn_type == E_CMD_WORD
		&& data->current->parent == NULL)
	{
		new_node->left = data->current;
		data->root = new_node;
		new_node->left->parent = new_node;
	}
	else if (data->current->tkn_type == E_CMD_WORD
		&& data->current->parent->tkn_type == E_PIPE)
		make_pipe_node(data, new_node);
	else
		return (-2);
	return (1);
}

void	ast_make_redir_node(t_data *data, t_ast *new_node)
{
	t_ast	*tmp;

	tmp = data->current;
	while (tmp->left)
		tmp = tmp->left;
	new_node->parent = tmp;
	tmp->left = new_node;
}

int	ast_redir_node(t_data *data, t_token *token)
{
	t_ast	*new_node;

	new_node = ast_create_node(data, token, E_REDIR);
	if (!new_node)
	{
		data->parse_err = -4;
		return (-1);
	}
	if (data->root == NULL)
		is_root_null(data, new_node);
	else if (data->current->tkn_type == E_REDIR
		|| data->current->tkn_type == E_CMD_WORD)
	{
		ast_make_redir_node(data, new_node);
		if (ft_strncmp(token->val, "<<", ft_strlen("<<")) == 0)
		{
			if (heredoc_parse(data, new_node) < 0)
				return (-3);
		}
	}
	else
		return (-2);
	return (1);
}
