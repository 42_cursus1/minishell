/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion_var_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 17:23:25 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 13:16:30 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
Tilde expansion in bash :
If a word begins with an unquoted tilde character (‘~’) :
	all characters up to the first unquoted slash 
		(or unquoted '$', or end of word if no slash/$ occurs)
	are considered a tilde-prefix, including the tilde

Tilde prefix is then considered a possible login name

If this login name is the null string (meaning no character after the tilde):
	-> the tilde is replaced with the value of the HOME shell variable. 
If HOME is unset, the home directory of the user executing the shell is 
	substituted instead. 
		unsupported in m-s : will work as if no "login", and just print the '~'

If it's not a null string:
	tilde-prefix is replaced with home directory of specified login name.

If the tilde-prefix is ‘~+’:
	the value of the shell variable PWD replaces the tilde-prefix.
If the tilde-prefix is ‘~-’:
	the value of the shell variable OLDPWD, if it is set, is substituted.

If the login name is invalid, or the tilde expansion fails:
	-> the word is left unchanged.

(and some other rules about builtins not implemented in m-s : 
	-> if ~ is followed by a number, etc.)

*/

char	*get_home_base(t_data *data, char *login)
{
	char	*home_dir;
	char	*home_base;
	char	*new_login;
	int		i;

	home_dir = env_value(data, "HOME");
	i = ft_strlen(home_dir);
	while (home_dir[i] != '/')
		i--;
	home_base = ft_substr(home_dir, 0, i + 1);
	new_login = ft_strjoin(home_base, login);
	free(home_dir);
	free(home_base);
	free(login);
	if ((chdir(new_login) == -1 && errno == EACCES) || chdir(new_login) == 0)
	{
		login = ft_strdup(new_login);
		free(new_login);
		return (login);
	}
	free(new_login);
	return (NULL);
}

char	*tilde_value(t_data *data, char *tilde_prefix, char *login)
{
	char	*env_val;

	env_val = NULL;
	login = ft_substr(tilde_prefix, 1, ft_strlen(tilde_prefix) - 1);
	if (tilde_prefix[1] == '+')
		env_val = ft_strdup(env_value(data, "PWD"));
	else if (tilde_prefix[1] == '-')
	{
		env_val = ft_strdup(env_value(data, "OLDPWD"));
		if (!env_val)
			env_val = ft_strdup(env_value(data, "PWD"));
	}
	else
	{
		env_val = get_home_base(data, login);
		if (!env_val)
			env_val = ft_strdup(tilde_prefix);
	}
	return (env_val);
}

char	*expand_tilde(t_data *data, t_ast *node, char *t_prefix, char *env_val)
{
	char	*tmp;
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	tmp = malloc(sizeof(char) * (ft_strlen(node->tkn_node->val)
				- ft_strlen(t_prefix) + ft_strlen(env_val) + 1));
	if (!tmp)
		return (data->parse_err = -1, NULL);
	while (node->tkn_node->val[i] != '~')
		tmp[j++] = node->tkn_node->val[i++];
	while (env_val[k] && (size_t)k < ft_strlen(env_val))
		tmp[j++] = env_val[k++];
	i += ft_strlen(t_prefix);
	while (node->tkn_node->val[i] && (size_t)i < ft_strlen(node->tkn_node->val))
		tmp[j++] = node->tkn_node->val[i++];
	tmp[j] = '\0';
	free(node->tkn_node->val);
	node->tkn_node->val = ft_strdup(tmp);
	free(tmp);
	return (node->tkn_node->val);
}

int	check_tilde_prefix(t_data *data, t_ast *node, char *t_prefix, char *login)
{
	char	*env_val;
	int		env_len;

	env_val = NULL;
	env_len = ft_strlen(t_prefix);
	if (env_len == 1)
	{
		node->tkn_node->val = expand_tilde(data, node, t_prefix, login);
		free(login);
	}
	if (env_len > 1)
	{
		env_val = tilde_value(data, t_prefix, login);
		node->tkn_node->val = expand_tilde(data, node, t_prefix, env_val);
		env_len = ft_strlen(env_val);
		free(env_val);
	}
	return (env_len);
}

int	tilde_xpand(t_data *data, t_ast *node)
{
	char	*tilde_prefix;
	int		i;
	int		prefix_len;
	char	*login;
	char	*tmp;

	i = 0;
	prefix_len = 0;
	tilde_prefix = NULL;
	login = NULL;
	while (node->tkn_node->val[i] && node->tkn_node->val[i] != '/')
		i++;
	tilde_prefix = ft_substr(node->tkn_node->val, 0, i);
	if (i == 1)
	{
		tmp = env_value(data, "HOME");
		login = ft_strdup(tmp);
		free(tmp);
	}
	prefix_len = check_tilde_prefix(data, node, tilde_prefix, login);
	free(tilde_prefix);
	return (prefix_len);
}
