/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 17:23:35 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 11:41:55 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	prod_rules(t_tkn_type non_term, t_token **cursor, t_data *data)
{
	int	result;
	int	(*prod[8])(t_token **cursor, t_data *data);

	if (non_term >= E_TOKEN_END || non_term <= E_NONTERMS)
		return (-3);
	prod[0] = cpl_cmd;
	prod[1] = list_cmd;
	prod[2] = pipeline;
	prod[3] = smpl_cmd;
	prod[4] = cmd_prefix;
	prod[5] = cmd_word;
	prod[6] = cmd_suffix;
	prod[7] = redir;
	result = prod[non_term - E_COMPLETE_CMD](cursor, data);
	return (result);
}

void	parse_init(t_data *data)
{
	data->parse_cursor = data->tkn_start;
	data->root = NULL;
	data->current = NULL;
	data->parse_stack = push(E_TOKEN_END, NULL);
	data->parse_stack = push(E_COMPLETE_CMD, data->parse_stack);
	data->parsing_result = 0;
	data->parse_err = 1;
}

/*
When parsing, We will need 2 cursors : 
- one to check what is on top of the parsing stack 
		-> the stack_cursor variable
- one along the string, that we will find with the prod_rules function

The prod_rules function is an array of pointer to functions, used by jgarcia,
	who explained its usefulness in avoiding an if-forrest


Parsing algorithm :
	either the token is a terminal, or it's a non-terminal
		if non-term :
			use parse table to find what is the next prod
			if there is no entry that matches token/pro rule -> syntax error
			error : if non-term is followed by end-of-stack -> return -3
			if match : 
				pop the NTL
				push the RHS of the rule, in reverse order
		else (== if terminal or if epsilon)
			if terminal :
				if string cursor matches the TL on top of the stack
					pop TL and advance string cursor
			if epsilon	:
				pop TL, do not advance cursor
			else : parsing error

At the end of the algorithm
	- the string cursor should be at the end of the string (== NULL)
	- the only token remaining on the parsing stack should be the end-token
		-> it is symbolized as '$' in the algo
		-> represented by E_TOKEN_END in the enum of mini-shell

*/

/*
Epsilon returns -98, not because error, but because available code
(and does not hamper expansion of grammar as it's a negative number)
*/

int	parsing(t_data *data)
{
	t_tkn_type	stack_cursor;

	if (data->parse_stack->tkn_type > E_NONTERMS)
	{
		stack_cursor = pop(&data->parse_stack);
		if (stack_cursor >= E_TOKEN_END)
			return (-3);
		data->parsing_result = prod_rules
			(stack_cursor, &data->parse_cursor, data);
		if (data->parsing_result < 0)
			return (data->parsing_result);
	}
	else
	{
		stack_cursor = pop(&data->parse_stack);
		if (stack_cursor == E_EPSILON)
			return (-98);
		if (data->parse_cursor->tkn_type == stack_cursor)
			data->parse_cursor = data->parse_cursor->nxt_tkn;
		else
			return (-2);
	}
	return (-98);
}

int	parse(t_data *data)
{
	int	parsing_status;

	parse_init(data);
	if (data->parse_stack == NULL)
		return (-1);
	while (stack_thru(data->parse_stack))
	{
		if (data->parse_stack->tkn_type == E_TOKEN_END)
			break ;
		parsing_status = parsing(data);
		if (parsing_status != -98)
			return (data->parse_err = parsing_status);
	}
	if (data->parse_cursor && data->parse_cursor->tkn_type != E_TOKEN_END)
		return (data->parse_err = -2);
	return (data->parse_err);
}
