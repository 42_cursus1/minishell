/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion_quotes.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 09:47:01 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 13:53:56 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	find_db_dol_len(char *quoted, int dol_idx)
{
	int	dol_len;

	dol_len = dol_idx + 1;
	if (quoted[dol_idx] == '$')
	{
		if (quoted[dol_idx + 1] == '?' || ft_isdigit(quoted[dol_idx + 1]))
			dol_len = 2;
		else
			dol_len = dollar_len(quoted, dol_len);
	}
	return (dol_len);
}

char	*db_quote_dollar(t_data *data, char *quoted)
{
	char	*prefix;
	char	*middle;
	char	*suffix;
	int		dol_idx;
	int		dol_len;

	dol_idx = 0;
	while (quoted[dol_idx] != 0 && quoted[dol_idx] != '$'
		&& quoted[dol_idx] != '\"')
		dol_idx++;
	dol_len = find_db_dol_len(quoted, dol_idx);
	if (dol_len == dol_idx + 1)
	{
		prefix = ft_substr(quoted, 0, ft_strlen(quoted));
		if (quoted != NULL)
			free(quoted);
		return (prefix);
	}
	prefix = ft_substr(quoted, 0, dol_idx);
	suffix = ft_substr(quoted, dol_len, ft_strlen(quoted) - dol_idx + dol_len);
	middle = expand_dollar_string(data, quoted, dol_idx, dol_len - dol_idx);
	quoted = join_strs(quoted, prefix, middle, suffix);
	return (quoted);
}

char	*rm_db_quotes(t_data *data, char *node_value, int q_idx)
{
	char	*quoted;
	char	*middle;
	int		idx;

	idx = q_idx + 1;
	middle = NULL;
	while (node_value[idx] && node_value[idx] != '\"')
		idx++;
	if (idx > 0)
	{
		quoted = ft_substr(node_value, q_idx + 1, idx - q_idx - 1);
		middle = db_quote_dollar(data, quoted);
	}
	return (middle);
}

char	*rm_sg_quotes(char *node_value, int q_idx)
{
	char	*middle;
	int		i;

	i = q_idx + 1;
	middle = NULL;
	while (node_value[i] && node_value[i] != '\'')
		i++;
	if (i > 0)
		middle = ft_substr(node_value, q_idx + 1, i - q_idx - 1);
	return (middle);
}
