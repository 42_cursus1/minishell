/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexing_word_tokens.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/28 14:15:18 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 10:49:39 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
According to Bash, WORD expansions are to occur in the following order :
	- tilde expansion		~ (as in HOME)
	- parameter expansion	${expression}
	- command substitution	$(command)
	- arithmetic expansion	$((expression))

	- field splitting

	- pathname expansion

	- quote removal

We are not performing the parameter expansion as {} are off-subject
Arithmetic expansion and field splitting are also off-subject
*/

int	check_doll(t_data *data)
{
	int	check;

	check = 1;
	if (data->cmd_line[data->idx + 1] == '{')
		check = -1;
	else if (data->cmd_line[data->idx + 1] == '(')
	{
		if (data->cmd_line[data->idx + 2] == '(')
			check = -1;
	}
	if (check < 1)
	{
		data->lex_err = -1;
		write(2, F_D_MAGENTA, ft_strlen(F_D_MAGENTA));
		write(2, "Minishell warning : We don't do parameter/arithmetic", 53);
		write(2, " expansion here !\n", 19);
		write(2, RST, ft_strlen(RST));
		write(2, "\n", 1);
	}
	return (check);
}

void	make_wd_tknz(t_data *data, int i)
{
	int	j;

	j = 0;
	data->idx--;
	while (i <= data->idx)
		data->word[j++] = data->cmd_line[i++];
	data->word[j] = '\0';
	add_token(data, E_WORD, data->word);
	data->idx++;
}

void	word_tknz(t_data *data)
{
	int		i;

	i = data->idx;
	while (data->cmd_line[data->idx] > 32
		&& is_operator(data->cmd_line[data->idx]) == 0)
	{
		if (data->cmd_line[data->idx] == '$')
		{
			if (check_doll(data) != 1)
				return ;
		}
		else if (data->cmd_line[data->idx] == '\''
			|| data->cmd_line[data->idx] == '\"')
		{
			data->idx = quote_tknz(data);
			if (data->lex_err < 0)
				return ;
		}
		data->idx++;
	}
	data->word = malloc(sizeof(char) * (data->idx - i + 1));
	if (!data->word)
		return ;
	make_wd_tknz(data, i);
	free(data->word);
}
