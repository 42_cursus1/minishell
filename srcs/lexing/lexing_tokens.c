/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexing_tokens.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/31 15:37:48 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 11:24:37 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	amper_tknz(t_data *data)
{
	data->lex_err = -1;
	write(2, F_D_MAGENTA, ft_strlen(F_D_MAGENTA));
	write(2, "Minishell warning : '&' is not authorized", 42);
	write(2, RST, ft_strlen(RST));
	write(2, "\n", 1);
}

void	pipe_tknz(t_data *data)
{
	if (data->cmd_line[data->idx + 1] == '|')
	{
		data->lex_err = -1;
		write(2, F_D_MAGENTA, ft_strlen(F_D_MAGENTA));
		write(2, "Minishell warning : Mandatory part, please avoid '||'", 54);
		write(2, RST, ft_strlen(RST));
		write(2, "\n", 1);
	}
	else
		add_token(data, E_PIPE, "|");
	data->idx++;
}

void	redir_in(t_data *data)
{
	if (data->cmd_line[data->idx + 1] != 0
		&& data->cmd_line[data->idx + 1] != '<')
		add_token(data, E_LESS_INFILE, "<");
	else if (data->cmd_line[data->idx + 1] == '<'
		&& data->cmd_line[data->idx + 2] != 0)
	{
		add_token(data, E_DLESS_HERE, "<<");
		data->idx++;
	}
	else
		data->lex_err = -5;
}

void	redir_out(t_data *data)
{
	if (data->cmd_line[data->idx + 1] != 0
		&& data->cmd_line[data->idx + 1] != '>')
		add_token(data, E_GREAT_OUTFILE, ">");
	else if (data->cmd_line[data->idx + 1] == '>'
		&& data->cmd_line[data->idx + 2] != 0)
	{
		add_token(data, E_DGREAT_OUTAPP, ">>");
		data->idx++;
	}
	else
		data->lex_err = -5;
}

void	redir_tknz(t_data *data)
{
	if (data->cmd_line[data->idx] == '<' && data->lex_err >= 0)
		redir_in(data);
	else if (data->cmd_line[data->idx] == '>' && data->lex_err >= 0)
		redir_out(data);
	data->idx++;
}
