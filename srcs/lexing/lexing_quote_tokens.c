/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexing_quote_tokens.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alex <alex@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/05 11:03:49 by acloos            #+#    #+#             */
/*   Updated: 2023/11/18 16:29:08 by alex             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	get_next_quote(t_data *data, char quote)
{
	int	i;

	i = data->idx + 1;
	while (data->cmd_line && data->cmd_line[i])
	{
		if (data->cmd_line[i] == quote)
			break ;
		i++;
	}
	if (!data->cmd_line[i])
		return (data->lex_err = -4);
	return (i);
}

int	quote_tknz(t_data *data)
{
	int	i;

	i = data->idx;
	if (data->cmd_line[data->idx] == '\"')
		i = get_next_quote(data, '\"');
	else if (data->cmd_line[data->idx] == '\'')
		i = get_next_quote(data, '\'');
	return (i);
}
