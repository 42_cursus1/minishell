/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_misc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/09 09:17:10 by acloos            #+#    #+#             */
/*   Updated: 2023/11/10 16:01:18 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	free_out(t_data *data)
{
	free(data->prompt);
	free_char_array(data->env_baz);
	free_char_array(data->env_cpy);
	free(data);
	rl_clear_history();
}

void	free_stack(t_stack *stack)
{
	if (!stack)
		return ;
	free_stack(stack->nxt_tkn);
	free(stack);
}

void	free_char_array(char **str)
{
	int	i;

	i = 0;
	if (!str)
		return ;
	while (str[i] != NULL)
	{
		free(str[i]);
		i++;
	}
	free(str);
}

void	free_exit(t_data *data, char **cmd, int exit_status)
{
	g_exit_status = exit_status;
	if (cmd != NULL)
		free_char_array(cmd);
	free_char_array(data->env_baz);
	free_char_array(data->env_cpy);
	free_stuff(data);
	free(data);
	exit(g_exit_status);
}

void	free_redir(t_redir *redir)
{
	if (!redir)
		return ;
	free_redir(redir->next_redir);
	free(redir);
}
