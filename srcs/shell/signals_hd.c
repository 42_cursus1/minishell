/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals_hd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/10 13:36:30 by acloos            #+#    #+#             */
/*   Updated: 2023/11/14 11:54:43 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	signal_hd(int signal)
{
	if (signal == SIGINT)
	{
		ioctl(0, TIOCSTI, "\n");
		g_exit_status = 130;
	}
}

void	signal_check_hd(void)
{
	signal(SIGINT, &signal_hd);
	signal(SIGQUIT, SIG_IGN);
}
