/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:52:22 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 11:47:09 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	g_exit_status = 0;

static t_data	*init_data_loop(t_data *data)
{
	data->prompt = get_prompt();
	data->cmd_line = readline(data->prompt);
	data->idx = 0;
	data->tkn_start = NULL;
	data->lex_err = 2;
	data->parse_err = 2;
	data->exec_err = 2;
	if (ft_strlen(data->cmd_line))
		add_history(data->cmd_line);
	return (data);
}

void	main_loop(t_data *data)
{
	int		status;

	status = lexer(data);
	lex_error(&status);
	if (status == 1)
		status = parse(data);
	parse_error(&status);
	if (status == 1 && data->root)
		status = exec(data);
	exec_err(&status);
	if (data->lex_err != (-1))
		free_stuff(data);
	else
	{
		if (data->tkn_start)
			free_token_list(data->tkn_start);
		free(data->prompt);
	}
}

int	main(int argc, char **argv, char **env)
{
	t_data	*data;

	splash();
	data = hellish_startup(argc, argv, env);
	while (1)
	{
		signal_setter();
		data = init_data_loop(data);
		if (!data->cmd_line)
		{
			write(1, "exit\n", 6);
			free(data->cmd_line);
			break ;
		}
		main_loop(data);
	}
	free_out(data);
	return (g_exit_status);
}

/*
{
	leak readline
	Memcheck:Leak
	...
	fun:readline
}
{
	leak add_history
	Memcheck:Leak
	...
	fun:add_history
}
*/
