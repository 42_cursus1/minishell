/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 17:23:52 by acloos            #+#    #+#             */
/*   Updated: 2023/11/20 13:54:31 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
Error management for lexer, parser and exec :
	- the main fct for each part will return :
		-> 1 if everything is ok
		-> a negative value in case of error
	- the return of each part is sent to its own error function
		-> depending on the value, a specific message will be printed
	- if the return was (1), program moves on to the next part
	- else, command loop is stopped, and prompts wait for a new cmd

*/

void	lex_error(int *status)
{
	if (*status >= 0)
		return ;
	if (*status == -1)
		write(2, "Lexer error : malloc failure\n", 30);
	else if (*status == -2)
		write(2, "Lexer error : not part of token list\n", 38);
	else if (*status == -3)
		write(2, "Lexer error : no value for node\n", 33);
	else if (*status == -4)
		write(2, "Lexer error : minishell only accepts closed quotes\n", 52);
	else if (*status == -5)
		write(2, "Lexer error : syntax issue\n", 28);
	*status = -99;
}

void	parse_error(int *status)
{
	if (*status >= 0)
		return ;
	if (*status == -1)
		write(2, "Parse error : malloc failure\n", 30);
	else if (*status == -2)
	{
		g_exit_status = 2;
		write(2, "Parse error : syntax failure\n", 30);
	}
	else if (*status == -3)
		write(2, "Parse error : out-of-bound token\n", 34);
	else if (*status == -4)
		write(2, "Parse error : AST structure is incorrect\n", 42);
	else if (*status == -5)
		write(2, "Parse error : issue with here-document\n", 40);
	else if (*status == -6)
		write(2, "Parse error : invalid command line\n", 36);
	*status = -99;
}

/*
Specific to exec :
A status of -1 means a message was printed already in the program
	-> either through perror, or a specific write()
*/

void	exec_err(int *status)
{
	if (*status >= 0)
		return ;
	if (*status == -1)
		*status = -99;
	else if (*status == -2)
		write(2, "Exec error : malloc failure", 28);
	else if (*status == -3)
		write(2, "Exec error : open() failure", 28);
	else if (*status == -4)
		write(2, "Exec error : pipe failure", 26);
	else if (*status == -5)
		write(2, "Exec error : execution failure", 31);
	*status = -99;
}
