/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   splash.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 15:07:36 by acloos            #+#    #+#             */
/*   Updated: 2023/11/10 13:39:49 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	mini_one(void)
{
	write(1, "\n", 2);
	write(1, F_D_GREEN, ft_strlen(F_D_GREEN));
	write(1, " ███╗   ███╗██╗███╗   ██╗██╗", 71);
	write(1, SLOW_BLINK F_D_RED, ft_strlen(SLOW_BLINK F_D_RED));
	write(1, "███████╗", 25);
	write(1, OFF_BLINKS F_D_GREEN, ft_strlen(OFF_BLINKS F_D_GREEN));
	write(1, "██╗  ██╗███████╗██╗     ██╗     \n", 74);
	write(1, " ████╗ ████║██║████╗  ██║██║", 77);
	write(1, SLOW_BLINK F_D_RED, ft_strlen(SLOW_BLINK F_D_RED));
	write(1, "██╔════╝", 25);
	write(1, OFF_BLINKS F_D_GREEN, ft_strlen(OFF_BLINKS F_D_GREEN));
	write(1, "██║  ██║██╔════╝██║     ██║     \n", 74);
	write(1, " ██╔████╔██║██║██╔██╗ ██║██║███████", 102);
	write(1, SLOW_BLINK, ft_strlen(SLOW_BLINK));
	write(1, "╗", 4);
	write(1, OFF_BLINKS, ft_strlen(OFF_BLINKS));
}

void	mini_two(void)
{
	write(1, "███████║█████╗  ██║     ██║     \n", 74);
	write(1, " ██║╚██╔╝██║██║██║╚██╗██║██║", 83);
	write(1, SLOW_BLINK F_D_RED, ft_strlen(SLOW_BLINK F_D_RED));
	write(1, "╚════██║", 25);
	write(1, OFF_BLINKS F_D_GREEN, ft_strlen(OFF_BLINKS F_D_GREEN));
	write(1, "██╔══██║██╔══╝  ██║     ██║     \n", 74);
	write(1, " ██║ ╚═╝ ██║██║██║ ╚████║██║", 77);
	write(1, SLOW_BLINK F_D_RED, ft_strlen(SLOW_BLINK F_D_RED));
	write(1, "███████║", 25);
	write(1, OFF_BLINKS F_D_GREEN, ft_strlen(OFF_BLINKS F_D_GREEN));
	write(1, "██║  ██║███████╗███████╗███████╗\n", 94);
	write(1, " ╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚═╝", 69);
	write(1, SLOW_BLINK F_D_RED, ft_strlen(SLOW_BLINK F_D_RED));
	write(1, "╚══════╝", 25);
	write(1, OFF_BLINKS F_D_GREEN, ft_strlen(OFF_BLINKS F_D_GREEN));
	write(1, "╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝", 93);
	write(1, RST, ft_strlen(RST));
}

void	ft_inhibiter(int sig)
{
	if (sig == SIGINT)
		signal(SIGINT, SIG_IGN);
	if (sig == SIGQUIT)
		signal(SIGQUIT, SIG_IGN);
}

void	signal_inhibit(void)
{
	struct sigaction	act;

	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGINT);
	sigaddset(&act.sa_mask, SIGQUIT);
	act.sa_handler = &ft_inhibiter;
	signal(SIGINT, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
}

void	splash(void)
{
	signal_inhibit();
	mini_one();
	mini_two();
	write(1, "\n\n\n", 4);
	usleep(250000);
}
