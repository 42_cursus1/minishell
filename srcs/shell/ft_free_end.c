/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_end.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 17:23:49 by acloos            #+#    #+#             */
/*   Updated: 2023/11/21 11:46:29 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	free_token_list(t_token *token)
{
	if (token == NULL)
		return ;
	free_token_list(token->nxt_tkn);
	free(token->val);
	free(token);
}

void	free_ast(t_ast *node)
{
	t_ast	*tmp;

	if (node == NULL)
		return ;
	tmp = node;
	free_ast(tmp->left);
	free_ast(tmp->right);
	free(tmp);
}

void	free_int_array(int **array)
{
	int	i;

	i = 0;
	if (!array)
		return ;
	while (array[i] != NULL)
	{
		free(array[i]);
		i++;
	}
	free(array);
}

void	free_stuff(t_data *data)
{
	data->current = data->root;
	free(data->prompt);
	if (data->lex_err < 2)
		free_token_list(data->tkn_start);
	if (data->parse_err < 2)
	{
		free_stack(data->parse_stack);
		if (data->parse_err <= -2)
			free_ast(data->root);
	}
	if (data->exec_err < 2)
	{
		free_int_array(data->exec->fd_array);
		data->current = data->root;
		data->exec->ast_pointer = data->root;
		free(data->exec);
		free_ast(data->root);
	}
}
