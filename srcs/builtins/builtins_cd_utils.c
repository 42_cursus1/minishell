/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_cd_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/09 10:32:17 by acloos            #+#    #+#             */
/*   Updated: 2023/11/14 17:24:17 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	cd_error(t_data *data, char *curpath, char **cmd_full, int flag)
{
	if (flag == 1)
	{
		data->exec_err = -1;
		write(2, ITALICS F_D_YELLOW, ft_strlen(ITALICS F_D_YELLOW));
		write(2, "minishell : cd : please provide one - and only one - ", 53);
		write(2, "path, be it relative or absolute.\n", 35);
		write(2, RST, ft_strlen(RST));
	}
	if (flag == 2)
		data->exec_err = -1;
	if (flag == 3)
	{
		write(2, "minishell : cd : ", 17);
		write(2, curpath, ft_strlen(curpath));
		perror(" ");
		free(curpath);
		data->exec_err = -1;
	}
	free_char_array(cmd_full);
	return (g_exit_status = 1);
}

char	*rm_dot_slash(char *curpath, int idx)
{
	char	*prev;
	char	*post;

	post = NULL;
	prev = ft_substr(curpath, 0, idx - 1);
	if (curpath[idx + 2] != '\0')
		post = ft_substr(curpath, idx + 1, ft_strlen(curpath));
	else
		post = ft_strdup("/");
	free(curpath);
	curpath = ft_strjoin(prev, post);
	free(prev);
	if (post != NULL)
		free(post);
	return (curpath);
}

char	*rm_dot_dot(t_data *data, char *curpath, int idx, int slash)
{
	char	*prev;
	char	*post;

	post = NULL;
	prev = ft_substr(curpath, 0, slash + 1);
	if (chdir(prev) == -1)
	{
		free(prev);
		data->exec_err = -1;
		perror("minishell : cd: " );
		return (NULL);
	}
	if (curpath[idx + 2] != '\0')
		post = ft_substr(curpath, idx + 3, ft_strlen(curpath));
	else
		post = ft_strdup("/");
	free(curpath);
	curpath = ft_strjoin(prev, post);
	free(prev);
	free(post);
	return (curpath);
}

char	*slash_the_dots(t_data *data, char *curpath, int i, int slash)
{
	if (curpath[i + 2] == '/' || curpath[i + 2] == '\0')
	{
		curpath = rm_dot_slash(curpath, i + 1);
	}
	else if (curpath[i + 2] == '.'
		&& (curpath[i + 3] == '/' || curpath[i + 3] == '\0'))
	{
		curpath = rm_dot_dot(data, curpath, i + 1, slash);
	}
	if (curpath == NULL)
		return (NULL);
	return (curpath);
}

char	*find_dots(t_data *data, char *curpath)
{
	int		i;
	int		slash;

	i = 0;
	slash = 0;
	while (curpath && curpath[i])
	{
		if (curpath[i] == '/' && curpath[i + 1] != '.')
			slash = i;
		else if (curpath[i] == '/' && curpath[i + 1] == '.')
		{
			if ((curpath[i + 2] == '/' || curpath[i + 2] == '\0')
				|| (curpath[i + 2] == '.'
					&& (curpath[i + 3] == '/' || curpath[i + 3] == '\0')))
			{
				curpath = slash_the_dots(data, curpath, i, slash);
				i = 0;
				slash = 0;
			}
			if (curpath == NULL)
				return (NULL);
		}
		i++;
	}
	return (curpath);
}
