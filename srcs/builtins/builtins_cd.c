/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins_cd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/21 17:12:29 by ryounssi          #+#    #+#             */
/*   Updated: 2023/11/21 09:06:45 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
cd : (minishell)
	- must have ONE directory operand
	- if d-o starts with '/' : curpath = d-o
	- else : curpath = PWD + '/' + d-o
	- then go through curpath from beginning to end
		- remove all the './'
		- if '../' -> ensure all of previous is a valid directory
			- if not, error msg and return
			- if it is remove '../' and previous component

from bash man :
	PWD
	This variable shall be set as specified in the DESCRIPTION. 
	If an application  sets or unsets the value of PWD, 
			the behavior of cd is unspecified.

In minishell here:
	- first we try to find $PWD
	- if it's not found, we use getcwd()
	- we don't try to re-set PWD
*/

char	*clean_slashes(t_data *data, char *curpath)
{
	char	*tmp;
	int		i;
	int		j;

	tmp = malloc(sizeof(char) * ft_strlen(curpath) + 1);
	if (!tmp)
	{
		data->exec_err = -2;
		return (NULL);
	}
	i = 0;
	j = 0;
	while (curpath[i])
	{
		while (curpath[i] == '/' && curpath[i + 1] == '/')
			i++;
		tmp[j++] = curpath[i++];
	}
	tmp[j] = '\0';
	free(curpath);
	curpath = ft_strdup(tmp);
	free(tmp);
	return (curpath);
}

int	chg_pwd(t_data *data, char *v_name, char *wdir)
{
	int		i;
	char	*tmp;

	i = 0;
	while (data->env_cpy[i])
	{
		if (ft_strncmp(data->env_cpy[i], v_name, ft_strlen(v_name)) == 0
			&& data->env_cpy[i][ft_strlen(v_name)] == '=')
		{
			free(data->env_cpy[i]);
			tmp = ft_strjoin(v_name, "=");
			data->env_cpy[i] = ft_strjoin(tmp, wdir);
			free(tmp);
			break ;
		}
		i++;
	}
	free(wdir);
	return (0);
}

int	change_dir(t_data *data, char *curpath, char *curdir)
{
	char	*xport_olddir;

	if (chdir(curpath) == 0)
	{
		if (getenv("OLDPWD") == NULL)
		{
			xport_olddir = ft_strjoin("OLDPWD=", curdir);
			add_var(data, xport_olddir, ft_strlen(xport_olddir));
			free(xport_olddir);
		}
		chg_pwd(data, "OLDPWD", curdir);
		if (getenv("PWD") != NULL)
			chg_pwd(data, "PWD", curpath);
		else
			free(curpath);
	}
	else
	{
		free(curdir);
		return (data->exec_err = -1);
	}
	return (1);
}

char	*get_curpath(char **cmd_full)
{
	char	*curpath;
	char	*curpath_tmp;

	if (cmd_full[1][0] == '/')
		curpath = ft_strdup(cmd_full[1]);
	else
	{
		curpath = getcwd(NULL, 0);
		curpath_tmp = ft_strjoin(curpath, "/");
		free(curpath);
		curpath = ft_strjoin(curpath_tmp, cmd_full[1]);
		free(curpath_tmp);
	}
	return (curpath);
}

int	built_cd(t_data *data, char **cmd_full)
{
	char	*curpath;
	char	*curdir;

	if (cmd_full == NULL || cmd_full[1] == NULL || cmd_full[2] != NULL)
		return (cd_error(data, NULL, cmd_full, 1));
	curpath = NULL;
	curdir = getcwd(NULL, 0);
	if (curdir == NULL)
		return (cd_error(data, NULL, cmd_full, 2));
	curpath = get_curpath(cmd_full);
	curpath = find_dots(data, curpath);
	if (curpath == NULL)
		return (cd_error(data, NULL, cmd_full, 2));
	curpath = clean_slashes(data, curpath);
	if (curpath == NULL)
		return (cd_error(data, NULL, cmd_full, 2));
	if (change_dir(data, curpath, curdir) == -1)
		return (cd_error(data, curpath, cmd_full, 3));
	free_char_array(cmd_full);
	return (0);
}
