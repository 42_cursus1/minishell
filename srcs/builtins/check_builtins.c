/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_builtins.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/31 11:16:36 by acloos            #+#    #+#             */
/*   Updated: 2023/11/14 17:23:03 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	sgl_builtins(t_data *data)
{
	char	**cmd;

	cmd = get_av(data->exec->ast_pointer);
	if (builtin_checker(data, cmd) != 2)
	{
		if (data->exec_err < 0)
			g_exit_status = 1;
		data->exec->ast_pointer = data->root;
		return (-1);
	}
	data->exec->ast_pointer = data->root;
	free_char_array(cmd);
	return (data->exec_err);
}

int	builtin_checker(t_data *data, char **cmd_full)
{
	if (ft_strncmp(cmd_full[0], "echo",
			ft_strlen("echo") + 1) == 0)
		return (built_echo(cmd_full));
	else if (ft_strncmp(cmd_full[0], "pwd",
			ft_strlen("pwd") + 1) == 0)
		return (built_pwd(data, cmd_full));
	else if (ft_strncmp(cmd_full[0], "cd",
			ft_strlen("cd") + 1) == 0)
		return (built_cd(data, cmd_full));
	else if (ft_strncmp(cmd_full[0], "env",
			ft_strlen("env") + 1) == 0)
		return (built_env(data, cmd_full));
	else if (ft_strncmp(cmd_full[0], "export",
			ft_strlen("export") + 1) == 0)
		return (built_export(data, cmd_full));
	else if (ft_strncmp(cmd_full[0], "unset",
			ft_strlen("unset") + 1) == 0)
		return (built_unset(data, cmd_full));
	else if (ft_strncmp(cmd_full[0], "exit",
			ft_strlen("exit") + 1) == 0)
		return (built_exit(data, cmd_full));
	return (2);
}
